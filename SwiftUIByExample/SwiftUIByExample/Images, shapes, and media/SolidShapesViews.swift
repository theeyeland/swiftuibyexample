//
//  SolidShapesViews.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-display-solid-shapes
struct SolidShapesViews: View {
    var body: some View {
        VStack {
            Rectangle()
                .fill(Color.red)
                .frame(width: 200, height: 200)
            Circle()
                .fill(Color.blue)
                .frame(width: 100, height: 100)
            RoundedRectangle(cornerRadius: 25)
                .fill(Color.green)
                .frame(width: 150, height: 100)
            Capsule()
                .fill(Color.yellow)
                .frame(width: 150, height: 100)
        }
    }
}

struct SolidShapesViews_Previews: PreviewProvider {
    static var previews: some View {
        SolidShapesViews()
    }
}
