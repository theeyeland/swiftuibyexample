//
//  UsingImage.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-images-using-image-views
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-the-way-an-image-is-fitted-to-its-space
struct UsingImage: View {
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    Image("full-english")
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(10)
                        .padding()
                    if let uiImage = UIImage(named: "fresh-baked-croissant") {
                        Image(uiImage: uiImage)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 300, height:120.0)
                            .clipped()
                            .cornerRadius(10)
                            .background(Color.blue)
                            .padding()
                    }

                    Image(systemName: "cloud.heavyrain.fill")
                        .font(.largeTitle)
                        .foregroundColor(Color.red)

                }
            }
            .navigationTitle("Image Usage")
        }
    }
}

struct UsingImage_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            UsingImage()
        }
    }
}
