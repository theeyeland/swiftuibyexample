//
//  SFSymbolsImagesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-render-images-using-sf-symbols
struct SFSymbolsImagesView: View {
    var body: some View {
        VStack {
            Image(systemName: "moon.stars.fill")
            Image(systemName: "wind.snow")
                .font(.largeTitle)
            Image(systemName: "cloud.heavyrain.fill")
                .font(.largeTitle)
                .foregroundColor(.red)
            Image(systemName: "cloud.sun.rain.fill")
                .renderingMode(.original) // actuvates original colors of multicolor SF image
                .font(.largeTitle)
                .padding()
                .background(Color.black)
                .clipShape(Circle())
        }
    }
}

struct SFSymbolsImagesView_Previews: PreviewProvider {
    static var previews: some View {
        SFSymbolsImagesView()
    }
}
