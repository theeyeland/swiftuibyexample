//
//  PartOfSolidShapeWithTrimView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-part-of-a-solid-shape-using-trim
struct PartOfSolidShapeWithTrimView: View {

    @State private var completionAmount: CGFloat = 0.0
       let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

    var body: some View {
        VStack {
            Circle()
                .trim(from: 0.5, to: 1.0)
                .frame(width: 100, height: 100)
            Circle()
                .trim(from: 0, to: 0.5)
                .frame(width: 100, height: 100)
            Circle()
                .trim(from: 0.25, to: 0.75)
                .frame(width: 100, height: 100)
            Circle()
                .trim(from: 0.0, to: 0.5)
                .frame(width: 100, height: 100)
                .rotationEffect(.degrees(-90))

            Rectangle()
                .trim(from: 0, to: completionAmount)
                .stroke(Color.red, lineWidth: 20)
                .frame(width: 200, height: 200)
                .rotationEffect(.degrees(-90))
                .onReceive(timer) { _ in
                    withAnimation {
                        if completionAmount == 1 {
                            completionAmount = 0
                        } else {
                            completionAmount += 0.2
                        }
                    }
                }
        }
    }
}

struct PartOfSolidShapeWithTrimView_Previews: PreviewProvider {
    static var previews: some View {
        PartOfSolidShapeWithTrimView()
    }
}
