//
//  ImagesAndViewsAsBackgroundsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-images-and-other-views-as-a-backgrounds
struct ImagesAndViewsAsBackgroundsView: View {
    var body: some View {
        VStack {
            Text("Hacking with Swift")
                .font(.system(size: 48))
                .padding(50)
                .background(
                    Image("fresh-baked-croissant")
                        .resizable()
                )
            Text("Hacking with Swift")
                .font(.largeTitle)
                .padding()
                .background(Circle()
                    .fill(Color.red)
                    .frame(width: 50, height: 50))

            Text("Hacking with Swift")
                .font(.largeTitle)
                .padding()
                .background(
                    Circle()
                        .fill(Color.red)
                        .frame(width: 100, height: 100)
                )
                .clipped()

            // NOTE: you can use any view can be a background
        }
    }
}

struct ImagesAndViewsAsBackgroundsView_Previews: PreviewProvider {
    static var previews: some View {
        ImagesAndViewsAsBackgroundsView()
    }
}
