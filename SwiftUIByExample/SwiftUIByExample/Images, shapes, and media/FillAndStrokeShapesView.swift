//
//  FillAndStrokeShapesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-fill-and-stroke-shapes-at-the-same-time
struct FillAndStrokeShapesView: View {
    var body: some View {
        VStack {
            // 1. option
            Circle()
                .strokeBorder(Color.black, lineWidth: 20)
                .background(Circle().fill(Color.blue))
                .frame(width: 150, height: 150)

            // 2. option
            ZStack {
                Circle()
                    .fill(Color.red)

                Circle()
                    .strokeBorder(Color.black, lineWidth: 20)
            }
            .frame(width: 150, height: 150)

            // using extension 
            Circle()
                .fill(Color.yellow, strokeBorder: Color.blue, lineWidth: 5)
                .frame(width: 150, height: 150)
        }
    }
}

struct FillAndStrokeShapes_Previews: PreviewProvider {
    static var previews: some View {
        FillAndStrokeShapesView()
    }
}

extension Shape {
    func fill<Fill: ShapeStyle, Stroke: ShapeStyle>(_ fillStyle: Fill, strokeBorder strokeStyle: Stroke, lineWidth: CGFloat = 1) -> some View {
        self
            .stroke(strokeStyle, lineWidth: lineWidth)
            .background(self.fill(fillStyle))
    }
}

extension InsettableShape {
    func fill<Fill: ShapeStyle, Stroke: ShapeStyle>(_ fillStyle: Fill, strokeBorder strokeStyle: Stroke, lineWidth: CGFloat = 1) -> some View {
        self
            .strokeBorder(strokeStyle, lineWidth: lineWidth)
            .background(self.fill(fillStyle))
    }
}
