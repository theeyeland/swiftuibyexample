//
//  TileImageView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-tile-an-image
struct TileImageView: View {
    var body: some View {
        VStack {
            Image("full-english")
                .resizable(resizingMode: .tile)
                .padding()
            Image("fresh-baked-croissant")
                .resizable(capInsets: EdgeInsets(top: 105, leading: 0, bottom: 100, trailing: 0), resizingMode: .tile)
                .padding()
        }
    }
}

struct TileImageView_Previews: PreviewProvider {
    static var previews: some View {
        TileImageView()
    }
}
