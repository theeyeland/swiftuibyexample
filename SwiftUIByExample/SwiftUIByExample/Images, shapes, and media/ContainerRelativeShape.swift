//
//  ContainerRelativeShape.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 02/06/2021.
//

import SwiftUI

// !!!! This is only for widhets - it is not working in iOS app
// https://www.hackingwithswift.com/quick-start/swiftui/when-should-you-use-containerrelativeshape
// New in iOS 14
struct ContainerRelativeShape: View {
    var body: some View {
        ZStack {
            ContainerRelativeShape() // this is usefull for Widgets only
                .foregroundColor(.blue)
//                .inset(by: 4)
//                .fill(Color.blue)

            Text("Hello, World!")
                .font(.title)
        }
        .frame(width: 300, height: 200)
        .background(Color.red)
        .clipShape(Capsule())
    }
}

struct ContainerRelativeShape_Previews: PreviewProvider {
    static var previews: some View {
        ContainerRelativeShape()
    }
}
