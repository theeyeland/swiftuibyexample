//
//  VideoPlayerView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI
import AVKit

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-play-movies-with-videoplayer
// New in iOS 14
struct VideoPlayerView: View {
    var body: some View {
        VStack {
            // Play video from bundle
//            VideoPlayer(player: AVPlayer(url:  Bundle.main.url(forResource: "video", withExtension: "mp4")!))
//                .frame(height: 400)
            VideoPlayer(player: AVPlayer(url:  URL(string: "https://bit.ly/swswift")!))
                .frame(height: 400)

            VideoPlayer(player: AVPlayer(url:  URL(string: "https://bit.ly/swswift")!)) {
                VStack {
                    Text("Watermark")
                        .foregroundColor(.black)
                        .background(Color.white.opacity(0.7))
                    Spacer()
                }
                .frame(width: 400, height: 300)
            }
        }
    }
}

struct VideoPlayerView_Previews: PreviewProvider {
    static var previews: some View {
        VideoPlayerView()
    }
}
