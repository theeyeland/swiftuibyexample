//
//  BorderAroundView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-a-border-around-a-view
struct BorderAroundView: View {
    var body: some View {
        VStack {
            Text("Hacking with Swift")
                .border(Color.green)
            Text("Hacking with Swift")
                .padding()
                .border(Color.green)
            Text("Hacking with Swift")
                .padding()
                .border(Color.red, width: 4)

            // for rounded cornets use RoundedRectangle
            Text("Hacking with Swift")
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.blue, lineWidth: 4)
                )
        }
    }
}

struct BorderAroundView_Previews: PreviewProvider {
    static var previews: some View {
        BorderAroundView()
    }
}
