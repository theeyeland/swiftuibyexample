//
//  3DRotationView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-rotate-a-view-in-3d

struct RotationIn3DView: View {

    @State private var rotation = 0.0

    var body: some View {
        VStack {
            Slider(value: $rotation, in: 0...360)
                .padding()
            Text("EPISODE LLVM")
                .font(.largeTitle)
                .foregroundColor(.yellow)
                .rotation3DEffect(.degrees(rotation), axis: (x: 1, y: 0, z: 0))
        }
    }
}

struct RotationIn3DView_Previews: PreviewProvider {
    static var previews: some View {
        RotationIn3DView()
    }
}
