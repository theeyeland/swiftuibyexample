//
//  AdjustingOffsetView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-the-position-of-a-view-using-its-offset
struct AdjustingOffsetView: View {
    var body: some View {
        VStack {
            Text("Home")
            Text("Options")
                .offset(y: 15)
                .padding(.bottom, 15)
            Text("Help")

            HStack {
                Text("Before")
                    .background(Color.red)
                    .offset(y: 15)

                // it has no effect on next modifiers
                Text("After")
                    .offset(y: 15)
                    .background(Color.red)
            }
            .padding()


            ZStack(alignment: .bottomTrailing) {
                Image("fresh-baked-croissant")
                Text("Photo credit: Paul Hudson.")
                    .padding(4)
                    .background(Color.black)
                    .foregroundColor(.white)
                    .offset(x: -5, y: -5) // allways add offset at the end so everything moves together
            }
        }
    }
}

struct AdjustingOffsetView_Previews: PreviewProvider {
    static var previews: some View {
        AdjustingOffsetView()
    }
}
