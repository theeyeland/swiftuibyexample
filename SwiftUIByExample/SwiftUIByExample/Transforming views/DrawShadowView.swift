//
//  DrawShadowView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-a-shadow-around-a-view

// A view modifier that detects shaking and calls a function of our choosing.
struct TheEyeLandShadowViewModifier: ViewModifier {

    let radius: CGFloat

    func body(content: Content) -> some View {
            content
                .shadow(radius: radius)
                .shadow(radius: radius)
                .shadow(radius: radius)
    }
}

// A View extension to make the modifier easier to use.
extension View {
    func theEyeLandShadow(radius: CGFloat = 5.0) -> some View {
        self.modifier(TheEyeLandShadowViewModifier(radius: radius))
    }
}

struct DrawShadowView: View {
    var body: some View {
        VStack {
            Text("Hacking with Swift")
                .padding()
                .shadow(radius: 5)
                .border(Color.red, width: 4)
            Text("Hacking with Swift")
                .padding()
                .shadow(color: .red, radius: 5)
                .border(Color.red, width: 4)

            // custom modifier to make stronger shadow
            Text("Hacking with Swift")
                .padding()
                .theEyeLandShadow(radius: 10)
                .border(Color.red, width: 4)

            Text("Hacking with Swift")
                .padding()
                .shadow(color: .red, radius: 5, x: 20, y: 20)
                .border(Color.red, width: 4)

            // by changing modifiers order the border is shadowed too.
            Text("Hacking with Swift")
                .padding()
                .border(Color.red, width: 4)
                .shadow(color: .red, radius: 5, x: 20, y: 20)
        }
    }
}

struct DrawShadowView_Previews: PreviewProvider {
    static var previews: some View {
        DrawShadowView()
    }
}
