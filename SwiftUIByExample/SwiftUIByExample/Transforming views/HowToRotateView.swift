//
//  HowToRotateView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-rotate-a-view
struct HowToRotateView: View {

    @State private var rotation = 0.0
    @State private var topLeadingRotation = 0.0

    var body: some View {
        VStack {
            Text("Up we go")
                .padding()
                .rotationEffect(.degrees(-45))
            Text("Up we go")
                .padding()
                .rotationEffect(.radians(.pi))

            Divider()

            VStack {
                Text("Center Rotation")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding()
                Slider(value: $rotation, in: 0...360)

                Text("Up we go")
                    .rotationEffect(.degrees(rotation))
            }

            Divider()

            VStack {
                Text("Top Leading Rotation")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding()
                Slider(value: $topLeadingRotation, in: 0...360)

                Text("Up we go")
                    .rotationEffect(.degrees(topLeadingRotation), anchor: .topLeading)
            }
        }
    }
}

struct HowToRotateView_Previews: PreviewProvider {
    static var previews: some View {
        HowToRotateView()
    }
}
