//
//  HowtoCustomButtonStyle.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI


// https://www.hackingwithswift.com/quick-start/swiftui/customizing-button-with-buttonstyle

struct BlueButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color(red: 0, green: 0, blue: 0.5))
            .foregroundColor(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: configuration.isPressed ? 0.5 : 1.0))
            .animation(.easeOut(duration: 0.2))
            .clipShape(Capsule())
    }
}

struct GrowingButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 1.2 : 1)
            .animation(.easeOut(duration: 0.2))
    }
}

struct HowtoCustomButtonStyle: View {
    var body: some View {
        VStack {
            // styling button
            Button("Press Me") {
                print("Button pressed!")
            }
            .padding()
            .background(Color(red: 0, green: 0, blue: 0.5))
            .clipShape(Capsule())

            Button("Press Me") {
                print("Button pressed!")
            }
            .buttonStyle(BlueButton()) // styling with custom style Cool

            Button("Press Me") {
                        print("Button pressed!")
                    }
            .buttonStyle(GrowingButton())
        }
    }
}

struct HowtoCustomButtonStyle_Previews: PreviewProvider {
    static var previews: some View {
        HowtoCustomButtonStyle()
    }
}
