//
//  StackModifierForAdvancedEffectsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-stack-modifiers-to-create-more-advanced-effects
struct StackModifierForAdvancedEffectsView: View {
    var body: some View {
        Text("Forecast: Sun")
            .font(.largeTitle)
            .foregroundColor(.white)
            .padding()
            .background(Color.red)
            .padding()
            .background(Color.orange)
            .padding()
            .background(Color.yellow)
    }
}

struct StackModifierForAdvancedEffectsView_Previews: PreviewProvider {
    static var previews: some View {
        StackModifierForAdvancedEffectsView()
    }
}
