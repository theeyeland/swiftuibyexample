//
//  AccentColorView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-the-accent-color-of-a-view

// how to Toggle Custom Style
struct ColoredToggleStyle: ToggleStyle {
    var label = ""
    var onColor = Color(UIColor.green)
    var offColor = Color(UIColor.systemGray5)
    var thumbColor = Color.white

    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Text(label)
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                RoundedRectangle(cornerRadius: 16, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 50, height: 29)
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 1, x: 0, y: 1)
                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.1))
            }
        }
        .font(.title)
        .padding(.horizontal)
    }
}

struct AccentColorView: View {

    @State private var opacity = 0.5
    @State private var onOff: Bool = true


    var body: some View {
        VStack {
            Button("Press Here") {
                print("Button pressed!")
                print("\(onOff)")
            }

            Slider(value: $opacity, in: 0...1)

            Toggle(isOn: $onOff) {
                Text("Show advanced options")
            }
            .toggleStyle(
                ColoredToggleStyle(label: "My Colored Toggle",
                                   onColor: .green,
                                   offColor: .red,
                                   thumbColor: Color(UIColor.systemTeal)))

            Text("\(onOff.description)")
        }
        .accentColor(.orange)
    }
}

struct AccentColorView_Previews: PreviewProvider {
    static var previews: some View {
        AccentColorView()
    }
}
