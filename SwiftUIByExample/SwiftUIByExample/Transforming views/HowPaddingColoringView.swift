//
//  HowPaddingColoringView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-color-the-padding-around-a-view
struct HowPaddingColoringView: View {
    var body: some View {
        VStack {
            Text("Hacking with Swift")
                .background(Color.red)
                .foregroundColor(.white)
                .padding()

            // to color whole area of padding use padding before coloring
            Text("Hacking with Swift")
                .padding()
                .background(Color.red)
                .foregroundColor(.white)
        }
    }
}

struct HowPaddingColoringView_Previews: PreviewProvider {
    static var previews: some View {
        HowPaddingColoringView()
    }
}
