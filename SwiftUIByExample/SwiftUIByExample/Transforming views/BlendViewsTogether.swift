//
//  BlendViewsTogether.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-blend-views-together
struct BlendViewsTogether: View {
    var body: some View {
        ZStack {
            Circle()
                .fill(Color.red)
                .frame(width: 200, height: 200)
                .offset(x: -50)
                .blendMode(.difference)

            Circle()
                .fill(Color.blue)
                .frame(width: 200, height: 200)
                .offset(x: 50)
                .blendMode(.difference)
        }
        .frame(width: 400)
    }
}

struct BlendViewsTogether_Previews: PreviewProvider {
    static var previews: some View {
        BlendViewsTogether()
    }
}
