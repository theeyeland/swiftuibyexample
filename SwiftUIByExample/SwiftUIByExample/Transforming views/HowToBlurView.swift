//
//  HowToBlurView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-blur-a-view

struct HowToBlurView: View {

    @State private var blurAmount: CGFloat = 0

    var body: some View {
        VStack {
            Image("fresh-baked-croissant")
                .resizable()
                .scaledToFit()
                .frame(width: 300, height: 300)
                .blur(radius: 20)

            Text("Welcome to my SwiftUI app")
                .blur(radius: 2)

            Text("Drag the slider to blur me")
                .padding()
                .blur(radius: blurAmount)

            Slider(value: $blurAmount, in: 0...20)
        }
    }
}

struct HowToBlurView_Previews: PreviewProvider {
    static var previews: some View {
        HowToBlurView()
    }
}
