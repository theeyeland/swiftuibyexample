//
//  HowToAdjustViewsTintigAndMore.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-views-by-tinting-and-desaturating-and-more
struct HowToAdjustViewsTintigAndMore: View {
    var body: some View {
        VStack {
            Image("fresh-baked-croissant")
                .colorMultiply(.red)
            Image("fresh-baked-croissant")
                .saturation(0.3)
            Image("fresh-baked-croissant")
                .contrast(0.5)
        }
    }
}

struct HowToAdjustViewsTintigAndMore_Previews: PreviewProvider {
    static var previews: some View {
        HowToAdjustViewsTintigAndMore()
    }
}
