//
//  HowToClipView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-clip-a-view-so-only-part-is-visible

struct HowToClipView: View {
    var body: some View {
        VStack {
            Button {
                print("Button was pressed!")
            } label: {
                Image(systemName: "bolt.fill")
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.green)
                    .clipShape(Circle())
            }
            Button {
                print("Pressed!")
            } label: {
                Image(systemName: "bolt.fill")
                    .foregroundColor(.white)
                    .padding(EdgeInsets(top: 10, leading: 30, bottom: 10, trailing: 30))
                    .background(Color.green)
                    .clipShape(Capsule())
            }
        }
    }
}

struct HowToClipView_Previews: PreviewProvider {
    static var previews: some View {
        HowToClipView()
    }
}
