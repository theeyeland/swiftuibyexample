//
//  HowToAdjustOpacity.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-the-opacity-of-a-view

struct HowToAdjustOpacity: View {
    @State private var opacity = 0.5

    var body: some View {
        VStack {
            Text("Now you see me")
                .padding()
                .background(Color.red)
                .opacity(opacity)
            Text(String(format: "%0.2f", opacity))
                .padding()

            Slider(value: $opacity, in: 0...1)
                .padding()
        }
    }
}

struct HowToAdjustOpacity_Previews: PreviewProvider {
    static var previews: some View {
        HowToAdjustOpacity()
    }
}
