//
//  ScaleViewUpAndDown.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-scale-a-view-up-or-down

//Tip: Scaling up a view won’t cause it to be redrawn at its new size, only stretched up or down. This means small text will look fuzzy, and small images might look pixellated or blurry.
struct ScaleViewUpAndDown: View {
    var body: some View {
        VStack {
            Text("Up we go")
                .scaleEffect(3)
                .frame(width: 300, height: 300)
            Divider()
            Text("Up we go")
                .scaleEffect(x: 1, y: 5) // scale independantly x and y
                .frame(width: 300, height: 300)

            Divider()
            Text("Up we go")
                .scaleEffect(2, anchor: .bottomTrailing) // scale with anchor
                .frame(width: 300, height: 300)
        }
    }
}

struct ScaleViewUpAndDown_Previews: PreviewProvider {
    static var previews: some View {
        ScaleViewUpAndDown()
    }
}
