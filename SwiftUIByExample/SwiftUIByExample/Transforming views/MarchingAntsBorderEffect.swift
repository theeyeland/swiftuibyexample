//
//  MarchingAntsBorderEffect.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-marching-ants-border-effect

//Using [10] for the dash parameter means SwiftUI will draw 10 points of our stroke then 10 points of space, repeating that pattern until the entire rectangle has been stroked. It’s an array because you can provide more than one value, such as [10, 5], to mean “10 points of stroke then a 5-point gap.”
struct MarchingAntsBorderEffect: View {
    @State private var phase: CGFloat = 0

    var body: some View {
        ZStack {
            Rectangle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4, dash: [10]))
            Rectangle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4, dash: [10], dashPhase: phase))
                .frame(width: 200, height: 200)
                .onAppear {
                    withAnimation(Animation.linear.repeatForever(autoreverses: false)) {
                        phase -= 20
                    }
                }
        }
    }
}

struct MarchingAntsBorderEffect_Previews: PreviewProvider {
    static var previews: some View {
        MarchingAntsBorderEffect()
    }
}
