//
//  HowToRoundCorners.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-round-the-corners-of-a-view
struct HowToRoundCorners: View {
    var body: some View {
        VStack {
            // set conrner radius
            Text("Round Me")
                .padding()
                .background(Color.red)
                .cornerRadius(25)

            // using clipShape
            Text("Round Me")
                .padding()
                .background(Color.red)
                .clipShape(Capsule())
        }
    }
}

struct HowToRoundCorners_Previews: PreviewProvider {
    static var previews: some View {
        HowToRoundCorners()
    }
}
