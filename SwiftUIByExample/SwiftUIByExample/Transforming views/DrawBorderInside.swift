//
//  DrawBorderInside.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-a-border-inside-a-view

//The strokeBorder() modifier insets the view by half your border width then applies the stroke, meaning that the entire BORDER IS DRAWN INSIDE THE VIEW.
//The stroke() modifier draws a border centered on the view’s edge, meaning that HALF THE BORDER IS INSIDE THE VIEW AND HALF OUTSIDE.
struct DrawBorderInside: View {
    var body: some View {
        VStack {
            Circle()
                .strokeBorder(Color.blue, lineWidth: 50)
                .frame(width: 200, height: 200)
                .padding()
            Circle()
                .stroke(Color.blue, lineWidth: 50)
                .frame(width: 200, height: 200)
                .padding()
        }
    }
}

struct DrawBorderInside_Previews: PreviewProvider {
    static var previews: some View {
        DrawBorderInside()
    }
}
