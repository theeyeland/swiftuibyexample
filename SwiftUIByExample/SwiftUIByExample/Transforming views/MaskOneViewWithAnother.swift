//
//  MaskOneViewWithAnother.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-mask-one-view-with-another
//masks it using the text “SWIFT!” so that the letters act as a cut out for the image:
struct MaskOneViewWithAnother: View {
    var body: some View {
        Image("fresh-baked-croissant")
            .resizable()
            .frame(width: 300, height: 300)
            .mask(
                Text("SWIFT!")
                    .font(.system(size: 72))
            )
    }
}

struct MaskOneViewWithAnother_Previews: PreviewProvider {
    static var previews: some View {
        MaskOneViewWithAnother()
    }
}
