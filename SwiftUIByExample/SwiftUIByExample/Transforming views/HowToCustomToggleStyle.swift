//
//  HowToCustomToggleStyle.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 26/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/customizing-toggle-with-togglestyle

struct CheckToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        Button {
            configuration.isOn.toggle()
        } label: {
            Label {
                configuration.label
            } icon: {
                Image(systemName: configuration.isOn ? "checkmark.circle.fill" : "circle")
                    .foregroundColor(configuration.isOn ? .accentColor : .secondary)
                    .accessibility(label: Text(configuration.isOn ? "Checked" : "Unchecked"))
                    .imageScale(.large)
            }
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct HowToCustomToggleStyle: View {
    @State private var isOn = false

    var body: some View {
        VStack {
            Toggle("Switch Me", isOn: $isOn)
                .toggleStyle(CheckToggleStyle())
            Toggle("Switch Me Other", isOn: $isOn) // other toggle style
                .toggleStyle(
                    ColoredToggleStyle(label: "My Colored Toggle",
                                       onColor: .green,
                                       offColor: .red,
                                       thumbColor: Color(UIColor.systemTeal)))
        }
    }
}

struct HowToCustomToggleStyle_Previews: PreviewProvider {
    static var previews: some View {
        HowToCustomToggleStyle()
    }
}
