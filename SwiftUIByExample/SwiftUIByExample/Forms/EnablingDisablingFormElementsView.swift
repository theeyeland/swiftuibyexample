//
//  EnablingDisablingFormElementsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/pickers-in-forms
struct EnablingDisablingFormElementsView: View {
    @State private var agreedToTerms = false

    var body: some View {
        Form {
            Section {
                Toggle("Agree to terms and conditions", isOn: $agreedToTerms)
            }

            Section(header: Text("Header")) {
                Button("Continue") {
                    print("Thank you!")
                }
                .disabled(agreedToTerms == false) // can be used for any View
            }
        }
    }

}

struct EnablingDisablingFormElementsView_Previews: PreviewProvider {
    static var previews: some View {
        EnablingDisablingFormElementsView()
    }
}
