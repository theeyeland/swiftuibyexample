//
//  ShowingHidingFormElementsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/showing-and-hiding-form-rows
struct ShowingHidingFormElementsView: View {
    @State private var showingAdvancedOptions = false
    @State private var enableLogging = false

    var body: some View {
        Form {
            Section {
                // COOL!!! .animation() - enables implicit animation when binding value changes
                Toggle("Show advanced options", isOn: $showingAdvancedOptions.animation())

                if showingAdvancedOptions {
                    Toggle("Enable logging", isOn: $enableLogging)
                }
            }
        }
    }
}

struct ShowingHidingFormElementsView_Previews: PreviewProvider {
    static var previews: some View {
        ShowingHidingFormElementsView()
    }
}
