//
//  PickersInForms.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/pickers-in-forms
struct PickersInForms: View {
    @State private var selectedStrength = "Mild"
    let strengths = ["Mild", "Medium", "Mature"]

    var body: some View {
        // you have to add NavigationView for default picker
        // becouse it pushes other view with options selection
        NavigationView {
            Form {
                Section {
                    Picker("Strength", selection: $selectedStrength) {
                        ForEach(strengths, id: \.self) {
                            Text($0)
                        }
                    }
//                    .pickerStyle(WheelPickerStyle()) // uncoment to use wheeled picker
//                    .pickerStyle(SegmentedPickerStyle()) // uncoment for segmented picker
                }
            }
            .navigationTitle("Select your cheese")
        }
    }
}

struct PickersInForms_Previews: PreviewProvider {
    static var previews: some View {
        PickersInForms()
    }
}
