//
//  FromsWithSectionsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/breaking-forms-into-sections
// Sections are identical to list sections
struct FromsWithSectionsView: View {
    @State private var enableLogging = false
    @State private var selectedColor = "Red"
    @State private var colors = ["Red", "Green", "Blue"]

    var body: some View {
        Form { // you can change Form to List and it will work
            Section(footer: Text("Note: Enabling logging may slow down the app")) {
                Picker("Select a color", selection: $selectedColor) {
                    ForEach(colors, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                Toggle("Enable Logging", isOn: $enableLogging)
            }

            Section {
                Button("Save changes") {
                    // activate theme!
                    print("click")
                }
            }
        }
    }
}

struct FromsWithSectionsView_Previews: PreviewProvider {
    static var previews: some View {
        FromsWithSectionsView()
    }
}
