//
//  BasicFormDesignView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/basic-form-design
struct BasicFormDesignView: View {
    @State private var enableLogging = false
       @State private var selectedColor = "Red"
       @State private var colors = ["Red", "Green", "Blue"]

       var body: some View {
        // NOTE you can use VStack too - Form is STACK!!!
           Form {
               Picker("Select a color", selection: $selectedColor) {
                   ForEach(colors, id: \.self) {
                       Text($0)
                   }
               }
               .pickerStyle(SegmentedPickerStyle()) // !!! uncomment to see diffrence

               Toggle("Enable Logging", isOn: $enableLogging)

               Button("Save changes") {
                   // activate theme!
               }
           }
       }
}

struct BasicFormDesignView_Previews: PreviewProvider {
    static var previews: some View {
        BasicFormDesignView()
    }
}
