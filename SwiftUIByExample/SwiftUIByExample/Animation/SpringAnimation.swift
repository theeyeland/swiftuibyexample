//
//  SpringAnimation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-spring-animation
// more about spring animation
// https://www.raywenderlich.com/books/ios-animations-by-tutorials/v6.0/chapters/4-springs
struct SpringAnimation: View {
    @State private var angle: Double = 0

    var body: some View {
        Button("Press here") {
            angle += 45
        }
        .padding()
        .rotationEffect(.degrees(angle))
//        .animation(.spring())

        // Spring = pruzina
        // stiffnes = tuhost, damping = tlmenie
        .animation(.interpolatingSpring(mass: 0.5, stiffness: 1, damping: 1, initialVelocity: 20))
    }
}

struct SpringAnimation_Previews: PreviewProvider {
    static var previews: some View {
        SpringAnimation()
    }
}
