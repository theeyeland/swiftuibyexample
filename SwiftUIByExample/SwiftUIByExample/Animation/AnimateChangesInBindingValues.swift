//
//  AnimateChangesInBindingValues.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-animate-changes-in-binding-values
struct AnimateChangesInBindingValues: View {
    @State private var showingWelcome = false

    var body: some View {
        VStack {
            Toggle("Toggle label", isOn: $showingWelcome.animation())
//            Toggle("Toggle label", isOn: $showingWelcome.animation(.spring()))
//            Toggle("Toggle label", isOn: $showingWelcome.animation(.interpolatingSpring(mass: 0.5, stiffness: 1, damping: 1, initialVelocity: 10)))

            if showingWelcome {
                Text("Hello World")
            }
        }
    }
}

struct AnimateChangesInBindingValues_Previews: PreviewProvider {
    static var previews: some View {
        AnimateChangesInBindingValues()
    }
}
