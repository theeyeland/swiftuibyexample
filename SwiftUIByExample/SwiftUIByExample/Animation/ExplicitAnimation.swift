//
//  ExplicitAnimation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// if you want animate exact view e.g here only button
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-an-explicit-animation

// implicit vs explicit
struct ExplicitAnimation: View {
    @State private var opacity = 1.0

    var body: some View {
        Button("Press here") {
//            withAnimation() {
            withAnimation(.linear(duration: 3)) {
                opacity -= 0.2
            }
        }
        .padding()
        .opacity(opacity)
    }
}

struct ExplicitAnimation_Previews: PreviewProvider {
    static var previews: some View {
        ExplicitAnimation()
    }
}
