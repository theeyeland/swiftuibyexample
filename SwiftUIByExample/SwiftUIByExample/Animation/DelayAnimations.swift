//
//  DelayAnimations.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-delay-an-animation
struct DelayAnimations: View {
    @State var rotation = 0.0

    var body: some View {
        Rectangle()
            .fill(Color.red)
            .frame(width: 200, height: 200)
            .rotationEffect(.degrees(rotation))
            .animation(Animation.easeInOut(duration: 3).delay(1)) // delaying implict animation
            .onTapGesture {
                rotation += 360
            }
    }
}

struct DelayAnimations_Previews: PreviewProvider {
    static var previews: some View {
        DelayAnimations()
    }
}
