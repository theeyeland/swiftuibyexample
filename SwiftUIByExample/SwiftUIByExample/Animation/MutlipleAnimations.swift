//
//  MutlipleAnimations.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-apply-multiple-animations-to-a-view
struct MutlipleAnimations: View {
    @State private var isEnabled = false
    @State var rotation = 0.0

    var body: some View {
        Button("Press Me") {
            isEnabled.toggle()
            rotation += 360
        }
        .foregroundColor(.white)
        .frame(width: 200, height: 200)
        .rotationEffect(.degrees(rotation))
        .background(isEnabled ? Color.green : Color.red)
        .animation(nil) // disable animation for backgroound color & rotation too
        .clipShape(RoundedRectangle(cornerRadius: isEnabled ? 100 : 0))
        .animation(.default) // implict animation - it animates evry modifier before unless .animation(nil)
    }
}

struct MutlipleAnimations_Previews: PreviewProvider {
    static var previews: some View {
        MutlipleAnimations()
    }
}
