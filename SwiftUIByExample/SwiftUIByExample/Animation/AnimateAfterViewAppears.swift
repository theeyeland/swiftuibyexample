//
//  AnimateAfterViewAppears.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-start-an-animation-immediately-after-a-view-appears
// here are som COOL extensions for View onAppear
// Create an immediate animation.
extension View {
    func animate(using animation: Animation = Animation.easeInOut(duration: 1), _ action: @escaping () -> Void) -> some View {
        onAppear {
            withAnimation(animation) {
                action()
            }
        }
    }
}

// Create an immediate, looping animation
extension View {
    func animateForever(using animation: Animation = Animation.easeInOut(duration: 1), autoreverses: Bool = false, _ action: @escaping () -> Void) -> some View {
        let repeated = animation.repeatForever(autoreverses: autoreverses)

        return onAppear {
            withAnimation(repeated) {
                action()
            }
        }
    }
}

struct AnimateAfterViewAppears: View {
    @State var scale: CGFloat = 1

    var body: some View {
        VStack {
            Circle()
                .frame(width: 200, height: 200)
                .scaleEffect(scale)
                .onAppear {
                    let baseAnimation = Animation.easeInOut(duration: 1)
                    let repeated = baseAnimation.repeatForever(autoreverses: true)

                    withAnimation(repeated) {
                        scale = 0.5
                    }
                }

            Circle()
                .frame(width: 200, height: 200)
                .scaleEffect(scale)
                .animateForever(autoreverses: true) { scale = 0.5 } // with extension
        }
    }
}

struct AnimateAfterViewAppears_Previews: PreviewProvider {
    static var previews: some View {
        AnimateAfterViewAppears()
    }
}
