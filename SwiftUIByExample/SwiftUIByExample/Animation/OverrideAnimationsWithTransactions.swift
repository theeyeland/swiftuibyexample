//
//  OverrideAnimationsWithTransactions.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-override-animations-with-transactions
struct OverrideAnimationsWithTransactions: View {
    @State private var isZoomed = false

       var body: some View {
           VStack {
               Button("Toggle Zoom") {
                var transaction = Transaction(animation: .linear) // override the animation in Zoom Text 1
                transaction.disablesAnimations = true

                withTransaction(transaction) {
                    isZoomed.toggle()
                }
               }

            Spacer()
                .frame(height: 100)

            Text("Zoom Text 1")
                   .font(.title)
                   .scaleEffect(isZoomed ? 3 : 1)
                   .animation(.easeInOut(duration: 2)) // becouse of transaktion in button the linear animation defined in Transaktion is Done not this .easeInOut

            Spacer()
                .frame(height: 100)

            Text("Zoom Text 2")
                .font(.title)
                .scaleEffect(isZoomed ? 3 : 1)
                .transaction { t in
                    t.animation = .none // you can turn out here the Transaktion Animation (Override the Transaction Animation)
                }
           }
       }
}

struct OverrideAnimationsWithTransactions_Previews: PreviewProvider {
    static var previews: some View {
        OverrideAnimationsWithTransactions()
    }
}
