//
//  AsymmetricTransitions.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-asymmetric-transitions
struct AsymmetricTransitions: View {
    @State private var showDetails = false

    var body: some View {
        VStack {
            Button("Press to show details") {
                withAnimation {
                    showDetails.toggle()
                }
            }

            if showDetails {
                Text("Details go here.")
                    .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .bottom)))
            }
        }

    }
}

struct AsymmetricTransitions_Previews: PreviewProvider {
    static var previews: some View {
        AsymmetricTransitions()
    }
}
