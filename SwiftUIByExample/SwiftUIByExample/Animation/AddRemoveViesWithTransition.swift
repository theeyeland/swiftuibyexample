//
//  AddRemoveViesWithTransition.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-and-remove-views-with-a-transition
struct AddRemoveViesWithTransition: View {
    @State private var showDetails = false

    var body: some View {
        VStack {
            Button("Press to show details") {
                withAnimation {
                    showDetails.toggle()
                }
            }

            if showDetails {
                // default transition
//                Text("Details go here.")
//                    .padding()

                // Moves in from the bottom
                Text("Details go here.")
                    .padding()
                    .transition(.move(edge: .trailing))

                // Moves in from leading out, out to trailing edge.
//                Text("Details go here.")
//                    .padding()
                    .transition(.slide)

                // Starts small and grows to full size.
//                Text("Details go here.")
//                    .padding()
//                    .transition(.scale)
            }
        }
    }
}

struct AddRemoveViesWithTransition_Previews: PreviewProvider {
    static var previews: some View {
        AddRemoveViesWithTransition()
    }
}
