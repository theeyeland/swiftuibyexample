//
//  CreateBasicAnimation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

//You can animate many other modifiers, such as 2D and 3D rotation, opacity, border, and more. For example, this makes a button that spins around and increases its border every time it’s tapped:

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-basic-animations
struct CreateBasicAnimation: View {
    @State private var scaleLinear: CGFloat = 1
    @State private var scaleEaseIn: CGFloat = 1
    @State private var scaleEaseOut: CGFloat = 1
    @State private var scaleEaseInOut: CGFloat = 1

    @State private var angle: Double = 0
    @State private var borderThickness: CGFloat = 1

    var body: some View {
        VStack {
            Button("Press here - linear") {
                scaleLinear = updateScale(scale: scaleLinear)
            }
            .padding()
            .scaleEffect(scaleLinear)
            .animation(.linear(duration: 1)) // !!!alsa called implicit animation and animates any changes within a view by previous modifiers

            Button("Press here - easeIn") {
                scaleEaseIn = updateScale(scale: scaleEaseIn)
            }
            .padding()
            .scaleEffect(scaleEaseIn)
            .animation(.easeIn)

            Button("Press here - easeOut") {
                scaleEaseOut = updateScale(scale: scaleEaseOut)
            }
            .padding()
            .scaleEffect(scaleEaseOut)
            .animation(.easeOut)

            Button("Press here - easeInOut") {
                scaleEaseInOut = updateScale(scale: scaleEaseInOut)
            }
            .padding()
            .scaleEffect(scaleEaseInOut)
            .animation(.easeInOut)

            Button("Press here") {
                        angle += 45
                        borderThickness += 1
                    }
                    .padding()
                    .border(Color.red, width: borderThickness)
                    .rotationEffect(.degrees(angle))
                    .animation(.easeIn)
        }
    }

    func updateScale(scale: CGFloat) -> CGFloat {
        if scale > 2 {
            return 1
        } else {
            return scale + 1
        }
    }
}

struct CreateBasicAnimation_Previews: PreviewProvider {
    static var previews: some View {
        CreateBasicAnimation()
    }
}
