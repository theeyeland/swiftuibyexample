//
//  CombineTransitions.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-combine-transitions

// "Implicit Member Expression e.g. .fadeAndMove"
// https://docs.swift.org/swift-book/ReferenceManual/Expressions.html#ID394
// extension for combined transition
extension AnyTransition {
    static var moveAndScale: AnyTransition {
        AnyTransition.move(edge: .bottom).combined(with: .scale)
    }

    public static func fadeAndMove(edge: Edge) -> AnyTransition {
        AnyTransition.opacity.combined(with: .move(edge: edge))
    }
}

struct CombineTransitions: View {
    @State private var showDetails = false

    var body: some View {
        VStack {
            Button("Press to show details") {
                withAnimation {
                    showDetails.toggle()
                }
            }

            if showDetails {
                Text("Details go here.")
                    .padding()
                     .transition(AnyTransition.opacity.combined(with: .slide))

                Text("move and scale.")
                    .padding()
                    .transition(.moveAndScale)

                Text("move and scale.")
                    .padding()
                    .transition(.fadeAndMove(edge: .trailing))
            }
        }

    }
}

struct CombineTransitions_Previews: PreviewProvider {
    static var previews: some View {
        CombineTransitions()
    }
}
