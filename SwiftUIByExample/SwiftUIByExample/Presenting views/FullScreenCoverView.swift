//
//  FullScreenCoverView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-present-a-full-screen-modal-view-using-fullscreencover

// NEW in iOS 14
struct FullScreenModalView: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        Button("Dismiss Modal") {
            presentationMode.wrappedValue.dismiss()
        }
    }
}

struct FullScreenCoverView: View {
    @State private var isPresented = false

       var body: some View {
           Button("Present!") {
               isPresented.toggle()
           }
           .fullScreenCover(isPresented: $isPresented, content: FullScreenModalView.init)
       }
}

struct FullScreenCoverView_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenCoverView()
    }
}
