//
//  ShowingPopoverView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-a-popover-view

// shows baloon message on iPadOS , and sheet on iOS
struct ShowingPopoverView: View {
    @State private var showingPopover = false

    var body: some View {
        Button("Show Menu") {
            showingPopover = true
        }
        .popover(isPresented: $showingPopover) {
            Text("Your content here")
                .font(.headline)
                .padding()
        }
    }
}

struct ShowingPopoverView_Previews: PreviewProvider {
    static var previews: some View {
        ShowingPopoverView()
    }
}
