//
//  PushNewView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-push-a-new-view-onto-a-navigationview
struct SecondView: View {
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            Text("This is the detail view")
            Button("pop view") {
                // programaticly pop the view
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct PushNewView: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: SecondView()) {
                    Text("Show Detail View")
                    
                }
                .navigationTitle("Navigation")
            }
        }
    }
}

struct PushNewView_Previews: PreviewProvider {
    static var previews: some View {
        PushNewView()
    }
}
