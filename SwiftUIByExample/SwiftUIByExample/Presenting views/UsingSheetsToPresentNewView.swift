//
//  UsingSheetsToPresentNewView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-present-a-new-view-using-sheets
struct SheetView: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        // we dont need navigation view here
        // I just added navigation view with toolbar for testing
        NavigationView {
            Button("Press to dismiss") {
                presentationMode.wrappedValue.dismiss()
            }
            .font(.title)
            .padding()
            .background(Color.black)
            .toolbar {
                Button("Cancel") {
                    presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }
}

struct UsingSheetsToPresentNewView: View {
    @State private var showingSheet = false

    var body: some View {
        Button("Show Sheet") {
            showingSheet.toggle()
        }
        .sheet(isPresented: $showingSheet) {
            SheetView()
        }
    }
}

struct UsingSheetsToPresentNewView_Previews: PreviewProvider {
    static var previews: some View {
        UsingSheetsToPresentNewView()
    }
}
