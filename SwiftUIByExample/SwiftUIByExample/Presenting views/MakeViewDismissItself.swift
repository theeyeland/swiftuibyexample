//
//  MakeViewDismissItself.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-a-view-dismiss-itself

// 1. approach using enviroment key for its presentation
struct DismissingView1: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        Button("Dismiss Me") {
            presentationMode.wrappedValue.dismiss()
        }
    }
}

// 2. approach using @Binding property - it will be shared with @State property cool
struct DismissingView2: View {
    @Binding var isPresented: Bool

    var body: some View {
        Button("Dismiss Me") {
            isPresented = false
        }
    }
}

struct MakeViewDismissItself: View {
    @State private var showingDetail = false

    var body: some View {
        Button("Show Detail") {
            showingDetail = true
        }
        .sheet(isPresented: $showingDetail) {
//            DismissingView1()
            DismissingView2(isPresented: $showingDetail)
        }
    }
}

struct MakeViewDismissItself_Previews: PreviewProvider {
    static var previews: some View {
        MakeViewDismissItself()
    }
}
