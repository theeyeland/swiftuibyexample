//
//  PushViewFromListRow.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-push-a-new-view-when-a-list-row-is-tapped
struct PlayerView: View {
    let name: String

    var body: some View {
        Text("Selected player: \(name)")
            .font(.largeTitle)
    }
}

struct PushViewFromListRow: View {
    let players = [
        "Roy Kent",
        "Richard Montlaur",
        "Dani Rojas",
        "Jamie Tartt",
    ]

    var body: some View {
        NavigationView {
//            List(players, id: \.self) { player in
//                NavigationLink(destination: PlayerView(name: player)) {
//                    Text(player)
//                }
//            }
            List {
                ForEach(players, id: \.self) { player in
                    NavigationLink(destination: PlayerView(name: player)) {
                        Text(player)
                    }
                }
            }
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Select a player")
        }
    }
}

struct PushViewFromListRow_Previews: PreviewProvider {
    static var previews: some View {
        PushViewFromListRow()
    }
}
