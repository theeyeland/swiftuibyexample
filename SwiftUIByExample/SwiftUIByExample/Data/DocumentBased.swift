//
//  DocumentBased.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-document-based-app-using-filedocument-and-documentgroup

// ios 14 only
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-export-files-using-fileexporter
struct DocumentBased: View {
    @Binding var document: TextFile

    @State private var showingExporter = false

    var body: some View {
        NavigationView {
            TextEditor(text: $document.text)
                .toolbar {
                    Button("export") {
                        showingExporter = true
                    }
                }
                .fileExporter(isPresented: $showingExporter, document: document, contentType: .plainText) { result in
                    switch result {
                    case .success(let url):
                        print("Saved to \(url)")
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
        }
    }
}

struct DocumentBased_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
