//
//  HandlingNSUserActivity.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 31/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-continue-an-nsuseractivity-in-swiftui
// Also see
// https://www.hackingwithswift.com/read/32/4/how-to-add-core-spotlight-to-index-your-app-content
struct HandlingNSUserActivity: View {

    var body: some View {
        Text("Hello World")
    }
}
