//
//  CreateExpandingListsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-expanding-lists
// NEW in iOS 14
struct Bookmark: Identifiable {
    let id = UUID()
    let name: String
    let icon: String
    var items: [Bookmark]? // this must be optional with same type is this struct (Bookmark)

    // some example websites
    static let apple = Bookmark(name: "Apple", icon: "1.circle")
    static let bbc = Bookmark(name: "BBC", icon: "square.and.pencil")
    static let swift = Bookmark(name: "Swift", icon: "bolt.fill")
    static let twitter = Bookmark(name: "Twitter", icon: "mic")

    // some example groups
    static let example1 = Bookmark(name: "Favorites", icon: "star", items: [Bookmark.apple, Bookmark.bbc, Bookmark.swift, Bookmark.twitter])
    static let example2 = Bookmark(name: "Recent", icon: "timer", items: [Bookmark.apple, Bookmark.bbc, Bookmark.swift, Bookmark.twitter])
    static let example3 = Bookmark(name: "Recommended", icon: "hand.thumbsup", items: [Bookmark.apple, Bookmark.bbc, Bookmark.swift, Bookmark.twitter])
}

struct CreateExpandingListsView: View {
    let items: [Bookmark] = [.example1, .example2, .example3]

        var body: some View {
            List(items, children: \.items) { row in // in children you define what rows to expand
                Image(systemName: row.icon)
                Text(row.name)
            }
        }
}

struct CreateExpandingListsView_Previews: PreviewProvider {
    static var previews: some View {
        CreateExpandingListsView()
    }
}
