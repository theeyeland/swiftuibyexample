//
//  BackgroundColorForListRowsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-set-the-background-color-of-list-rows-using-listrowbackground
struct BackgroundColorForListRowsView: View {
    var body: some View {
        List {
            ForEach(0..<10) {
                Text("Row \($0)")
            }
            .listRowBackground(Color.red)
        }
    }
}

struct BackgroundColorForListRowsView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundColorForListRowsView()
    }
}
