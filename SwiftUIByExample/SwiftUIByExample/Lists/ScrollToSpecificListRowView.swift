//
//  ScrollToSpecificListRowView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-scroll-to-a-specific-row-in-a-list
// NEW in iOS 14
struct ScrollToSpecificListRowView: View {
    var body: some View {
        ScrollViewReader { proxy in
            VStack {
                Button("Jump to #50") {
                    withAnimation(.linear(duration: 30)) {
                        proxy.scrollTo(50, anchor: .top)
                    }
                }

                List(0..<100, id: \.self) { i in
                    Text("Example \(i)")
                        .id(i)
                }
            }
        }
    }
}

struct ScrollToSpecificListRowView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollToSpecificListRowView()
    }
}
