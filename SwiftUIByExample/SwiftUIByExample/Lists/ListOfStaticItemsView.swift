//
//  ListOfStaticItemsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-list-of-static-items
struct Pizzeria: View {
    let name: String

    var body: some View {
        Text("Restaurant: \(name)")
    }
}
struct PizzeriaBig: View {
    let name: String

    var body: some View {
        Text("Restaurant: \(name)")
            .font(.largeTitle)
            .foregroundColor(.yellow)
    }
}

struct ListOfStaticItemsView: View {
    var body: some View {
        List {
            Pizzeria(name: "Joe's Original")
            Pizzeria(name: "The Real Joe's Original")
            PizzeriaBig(name: "Original Joe's And Zoltan's")
        }
    }
}

struct ListOfStaticItemsView_Previews: PreviewProvider {
    static var previews: some View {
        ListOfStaticItemsView()
    }
}
