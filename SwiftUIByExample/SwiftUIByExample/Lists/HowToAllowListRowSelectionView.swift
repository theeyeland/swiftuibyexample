//
//  HowToAllowListRowSelectionView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-allow-row-selection-in-a-list
struct HowToAllowListRowSelectionView: View {
    @State private var selection: String? // use this for single selection and must be optional and same type as row value type

//    @State private var selection = Set<String>() // // use this for multi selection and must be set with same type as row value type

    let names = [
        "Cyril",
        "Lana",
        "Mallory",
        "Sterling"
    ]

    var body: some View {
        NavigationView {
            List(names, id: \.self, selection: $selection) { name in
                Text(name)
            }
            .navigationTitle("List Selection")
            .toolbar {
                EditButton()
            }
            .onChange(of: selection) { newValue in
                print("selectin changed to \(newValue ?? "")!")
            }
        }
    }
}

struct HowToAllowListRowSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        HowToAllowListRowSelectionView()
    }
}
