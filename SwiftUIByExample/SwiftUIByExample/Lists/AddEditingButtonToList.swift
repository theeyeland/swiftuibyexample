//
//  AddEditingButtonToList.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-enable-editing-on-a-list-using-editbutton
struct AddEditingButtonToList: View {
    @State private var users = ["Paul", "Taylor", "Adele with very long text to check how it works"]

    var body: some View {
        NavigationView {
            List {
                ForEach(users, id: \.self) { user in
                    Text(user)
                        .font(.largeTitle)
                }
                .onDelete(perform: delete)
            }
            .toolbar {
                EditButton()
            }
        }
    }

    func delete(at offsets: IndexSet) {
        users.remove(atOffsets: offsets)
    }
}

struct AddEditingButtonToList_Previews: PreviewProvider {
    static var previews: some View {
        AddEditingButtonToList()
    }
}
