//
//  ImplicitStackingListView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-implicit-stacking
// An example struct to hold some data
struct User: Identifiable {
    let id = UUID()
    let username = "Anonymous"
}

struct ImplicitStackingListView: View {
    let users = [User(), User(), User()]

        var body: some View {
            List(users) { user in
                // !!! here HStack is added implicitly to put image and text together
                Image("full-english")
                    .resizable()
                    .frame(width: 40, height: 40)
                Text(user.username)
            }
        }
}

struct ImplicitStackingListView_Previews: PreviewProvider {
    static var previews: some View {
        ImplicitStackingListView()
    }
}
