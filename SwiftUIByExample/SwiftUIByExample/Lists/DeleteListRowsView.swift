//
//  DeleteListRowsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-let-users-delete-rows-from-a-list
struct DeleteListRowsView: View {
    @State private var users = ["Paul", "Taylor", "Adele"]

    var body: some View {
        NavigationView {
            List {
                ForEach(users, id: \.self) { user in
                    Text(user)
                }
                .onDelete(perform: delete) // modifier as on ForEach
//                .onDelete(perform: { indexSet in
//                    users.remove(atOffsets: indexSet)
//                })
            }
        }
    }

    func delete(at offsets: IndexSet) {
        users.remove(atOffsets: offsets)
    }
}

struct DeleteListRowsView_Previews: PreviewProvider {
    static var previews: some View {
        DeleteListRowsView()
    }
}
