//
//  HowToAddListSectionsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

struct TaskRow: View {
    var body: some View {
        Text("Task data goes here")
    }
}

struct HeaderRow: View {

    var body: some View {
        HStack {
            VStack {
                Text("Task data goes here").font(.headline)
                Text("Task data goes here").font(.subheadline)
            }
            Spacer()
            Text("+35€").foregroundColor(.green)
        }
    }
}

struct HowToAddListSectionsView: View {
    var body: some View {
        List {
            Section(header: HeaderRow()) {
                TaskRow()
                TaskRow()
                TaskRow()
            }

            Section(header: Text("Other tasks"), footer: Text("End").font(.footnote)) {
                TaskRow()
                TaskRow()
                TaskRow()
            }
        }
    }
}

struct HowToAddListSectionsView_Previews: PreviewProvider {
    static var previews: some View {
        HowToAddListSectionsView()
    }
}
