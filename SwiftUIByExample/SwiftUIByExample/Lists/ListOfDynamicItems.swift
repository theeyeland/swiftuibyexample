//
//  ListOfDynamicItems.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-list-of-dynamic-items
// A struct to store exactly one restaurant's data.
struct Restaurant: Identifiable {
    let id = UUID()
    let name: String
    let isBig: Bool
}

// A view that shows the data for one Restaurant.
struct RestaurantRow: View {
    var restaurant: Restaurant

    var body: some View {
        if restaurant.isBig {
            Text("Come and eat at \(restaurant.name)")
                .font(.title)
        } else {
            Text("Come and eat at \(restaurant.name)")
        }
    }
}

struct ListOfDynamicItems: View {
    let restaurants = [
        Restaurant(name: "Joe's Original", isBig: true),
        Restaurant(name: "The Real Joe's Original", isBig: false),
        Restaurant(name: "Original Joe's", isBig: false)
    ]

    var body: some View {
        List(restaurants) { restaurant in
            RestaurantRow(restaurant: restaurant)
        }
    }
}

struct ListOfDynamicItems_Previews: PreviewProvider {
    static var previews: some View {
        ListOfDynamicItems()
    }
}
