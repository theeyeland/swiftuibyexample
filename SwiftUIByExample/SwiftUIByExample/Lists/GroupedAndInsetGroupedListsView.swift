//
//  GroupedAndInsetGroupedListsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-grouped-and-inset-grouped-lists
struct ExampleRow: View {
    var body: some View {
        Text("Example Row")
    }
}

struct GroupedAndInsetGroupedListsView: View {
    var body: some View {
        List {
            Section(header: Text("Examples")) {
                ExampleRow()
                ExampleRow()
                ExampleRow()
            }
        }
        .listStyle(GroupedListStyle())

//         !!!! UNCOMMENT to check InsetGroupedListStyle
        List(0..<100) { i in
            Text("Row \(i)")
        }
        .listStyle(InsetGroupedListStyle())
    }
}

struct GroupedAndInsetGroupedListsView_Previews: PreviewProvider {
    static var previews: some View {
        GroupedAndInsetGroupedListsView()
    }
}
