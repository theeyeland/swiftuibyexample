//
//  MoveListRowsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-let-users-move-rows-in-a-list
struct MoveListRowsView: View {
    @State private var users = ["Paul", "Taylor", "Adele with very long text to check how it works"]

        var body: some View {
            NavigationView {
                List {
                    ForEach(users, id: \.self) { user in
                        Text(user)
                            .font(.largeTitle)
                    }
                    .onMove(perform: move)
                }
                .toolbar {
                    EditButton()
                }
            }
        }

        func move(from source: IndexSet, to destination: Int) {
            users.move(fromOffsets: source, toOffset: destination)
        }
}

struct MoveListRowsView_Previews: PreviewProvider {
    static var previews: some View {
        MoveListRowsView()
    }
}
