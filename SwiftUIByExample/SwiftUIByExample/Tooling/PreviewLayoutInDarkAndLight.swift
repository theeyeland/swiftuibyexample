//
//  PreviewLayoutInDarkAndLight.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 01/06/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-preview-your-layout-in-light-and-dark-mode

struct PreviewLayoutInDarkAndLight: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PreviewLayoutInDarkAndLight_Previews: PreviewProvider {
    static var previews: some View {
        // preview in dark mode
//        PreviewLayoutInDarkAndLight()
//            .preferredColorScheme(.dark)
        // preview in dark mode and light mode
//        ForEach(ColorScheme.allCases, id: \.self) {
//            PreviewLayoutInDarkAndLight().preferredColorScheme($0)
//                }

        // more simplified
        ForEach(ColorScheme.allCases, id: \.self, content: PreviewLayoutInDarkAndLight().preferredColorScheme)
    }
}
