//
//  PreviewInNavigation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 01/06/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-preview-your-layout-in-a-navigation-view

struct PreviewInNavigation: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            .navigationTitle("Welcome")
//            .navigationBarTitleDisplayMode(.inline)
    }
}

struct PreviewInNavigation_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PreviewInNavigation()
        }
    }
}
