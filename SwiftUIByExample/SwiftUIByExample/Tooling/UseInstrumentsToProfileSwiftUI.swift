//
//  UseInstrumentsToProfileSwiftUI.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 01/06/2021.
//

import SwiftUI
import Combine

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-instruments-to-profile-your-swiftui-code-and-identify-slow-layouts

class FrequentUpdater: ObservableObject {
    let objectWillChange = PassthroughSubject<Void, Never>()
    var timer: Timer?

    init() {
        timer = Timer.scheduledTimer(
            withTimeInterval: 0.01,
            repeats: true
        ) { _ in
            self.objectWillChange.send()
        }
    }
}

struct UseInstrumentsToProfileSwiftUI: View {
    @ObservedObject var updater = FrequentUpdater()
    @State private var tapCount = 0

    var body: some View {
        VStack {
            Text("\(UUID().uuidString)")

            Button("Tap count: \(tapCount)") {
                tapCount += 1
            }
        }
    }
}

struct UseInstrumentsToProfileSwiftUI_Previews: PreviewProvider {
    static var previews: some View {
        UseInstrumentsToProfileSwiftUI()
    }
}
