//
//  PreviewLayoutWithDynamicTypeSizes.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 01/06/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-preview-your-layout-at-different-dynamic-type-sizes

struct PreviewLayoutWithDynamicTypeSizes: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PreviewLayoutWithDynamicTypeSizes_Previews: PreviewProvider {
    static var previews: some View {
        PreviewLayoutWithDynamicTypeSizes()
            .environment(\.sizeCategory, .extraSmall)

        PreviewLayoutWithDynamicTypeSizes()

        PreviewLayoutWithDynamicTypeSizes()
                    .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
    }
}
