//
//  PreviewInDiffrentDevices.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 01/06/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-preview-your-layout-in-different-devices

struct PreviewInDiffrentDevices: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PreviewInDiffrentDevices_Previews: PreviewProvider {
    static var previews: some View {
        PreviewInDiffrentDevices()
            .previewDevice(PreviewDevice(rawValue: "iPhone 12"))
            .previewDisplayName("iPhone 12")
        PreviewInDiffrentDevices()
            .previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro Max"))
            .previewDisplayName("iPhone 12 Pro Max")
    }
}
