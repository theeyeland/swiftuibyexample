//
//  ControlLayoutPriorityView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-control-layout-priority-using-layoutpriority
struct ControlLayoutPriorityView: View {
    var body: some View {
        HStack {
            Spacer()
            Text("The rain Spain falls mainly on the Spaniards.")
                .layoutPriority(0)
            Text("Knowledge is power, France is bacon.")
                .layoutPriority(1)
            Spacer()
        }
        .font(.largeTitle)
    }
}

struct ControlLayoutPriorityView_Previews: PreviewProvider {
    static var previews: some View {
        ControlLayoutPriorityView()
    }
}
