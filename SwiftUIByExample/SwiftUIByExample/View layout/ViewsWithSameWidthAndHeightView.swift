//
//  ViewsWithSameWidthAndHeightView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-two-views-the-same-width-or-height
struct ViewsWithSameWidthAndHeightView: View {
    var body: some View {
        VStack {
            HStack {
                Text("This is a short string.")
                    .padding()
                    .frame(maxHeight: .infinity)
                    .background(Color.red)

                Text("This is a very long string with lots and lots of text that will definitely run across multiple lines because it's just so long.")
                    .padding()
                    .frame(maxHeight: .infinity)
                    .background(Color.green)
            }
            .fixedSize(horizontal: false, vertical: true)
            .frame(maxHeight: 200)

            VStack {
                Button("Log in") { }
                    .foregroundColor(.white)
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.red)
                    .clipShape(Capsule())

                Button("Reset Password") { }
                    .foregroundColor(.white)
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.red)
                    .clipShape(Capsule())
            }
            .fixedSize(horizontal: true, vertical: false)

            HStack {
                Button("Log in") { }
                    .foregroundColor(.white)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 55)
                    .background(Color.yellow)
                    .clipShape(Capsule())
                    .padding(.leading, 8)

                Button("Reset Password") { }
                    .foregroundColor(.white)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 55)
                    .background(Color.yellow)
                    .clipShape(Capsule())
                    .padding(.trailing, 8)
            }
//            .fixedSize(horizontal: false, vertical: true)
        }
    }
}

struct ViewsWithSameWidthAndHeightView_Previews: PreviewProvider {
    static var previews: some View {
        ViewsWithSameWidthAndHeightView()
    }
}
