//
//  CustomFrameView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-give-a-view-a-custom-frame
struct CustomFrameView: View {
    var body: some View {
        Button {
            print("Button tapped")
        } label: {
            Text("Welcome")
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 55) // you have to use this line if we want the whole button clickable
//                .background(Color.blue)
                .foregroundColor(Color.white)
                .font(.largeTitle)
//                .cornerRadius(10)
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 55)
        .background(Color.yellow)
        .cornerRadius(10)
        .padding(8)

        Text("Please log in")
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
            .font(.largeTitle)
            .foregroundColor(.white)
            .background(Color.red)
            .ignoresSafeArea()
    }
}

struct CustomFrameView_Previews: PreviewProvider {
    static var previews: some View {
        CustomFrameView()
    }
}
