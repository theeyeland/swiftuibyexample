//
//  ReturnDiffrentViewTypesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-return-different-view-types
struct ReturnDiffrentViewTypesView: View {

    // when returning some View in group - opaque type needs that you return only same type - so you can't return Image or Text : HERE are options how to fix it
    // 1. option - hide it in group
    var tossResult: some View {
        Group {
            if Bool.random() {
                Image("fresh-baked-croissant")
                    .resizable()
                    .scaledToFit()
            } else {
                Text("Better luck next time")
                    .font(.title)
            }
        }
        .frame(width: 200, height: 150)
    }

    // 2. option - use Any View - but it has parfomance costs
    var tossResult2: some View {
        if Bool.random() {
            return AnyView(Image("fresh-baked-croissant").resizable().scaledToFit()) } else {
                return AnyView(Text("Better luck next time").font(.title))
            }
    }

    // 3. option - use @ViewBuilder
    @ViewBuilder var tossResult3: some View {
            if Bool.random() {
                Image("fresh-baked-croissant")
                    .resizable()
                    .scaledToFit()
            } else {
                Text("Better luck next time")
                    .font(.title)
            }
        }

    // 4. options - separate views to smaller views see TossResult View

    var body: some View {
        VStack {
            Text("Coin Flip")
                .font(.largeTitle)

            tossResult
            tossResult2
                .frame(width: 200, height: 150)
            tossResult3
                .frame(width: 200, height: 150)
            TossResultView()
                .frame(width: 200, height: 150)
        }
    }
}

struct TossResultView: View {
    var body: some View {
        if Bool.random() {
            Image("fresh-baked-croissant")
                .resizable()
                .scaledToFit()
        } else {
            Text("Better luck next time")
                .font(.title)
        }
    }
}

struct ReturnDiffrentViewTypesView_Previews: PreviewProvider {
    static var previews: some View {
        ReturnDiffrentViewTypesView()
    }
}
