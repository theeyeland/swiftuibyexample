//
//  IgnoreSafeAreaView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-place-content-outside-the-safe-area
struct IgnoreSafeAreaView: View {
    var body: some View {
        Text("Hello World")
            .frame(minWidth: 100, maxWidth: .infinity, minHeight: 100, maxHeight: .infinity)
            .font(.largeTitle)
            .background(Color.red)
            .foregroundColor(Color.white)
            .ignoresSafeArea()
    }
}

struct IgnoreSafeAreaView_Previews: PreviewProvider {
    static var previews: some View {
        IgnoreSafeAreaView()
    }
}
