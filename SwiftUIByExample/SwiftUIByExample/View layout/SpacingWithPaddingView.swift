//
//  SpacingWithPaddingView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-control-spacing-around-individual-views-using-padding
struct SpacingWithPaddingView: View {
    var body: some View {
        VStack {
            Text("SwiftUI")
                .padding([.leading, .trailing], 50)
                .background(Color.orange)
            Text("rocks")
                .padding(.all)
                .background(Color.yellow)
        }
    }
}

struct SpacingWithPaddingView_Previews: PreviewProvider {
    static var previews: some View {
        SpacingWithPaddingView()
    }
}
