//
//  CreateViewsInLoopForEachView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 18/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-views-in-a-loop-using-foreach
struct SimpleGameResult {
    let id = UUID()
    let score: Int
}

struct SimpleGameResultAutoId: Identifiable {
    let id = UUID()
    let score: Int
}

struct CreateViewsInLoopForEachView: View {

    let colors: [Color] = [.red, .green, .blue]

    let results = [
            SimpleGameResult(score: 8),
            SimpleGameResult(score: 5),
            SimpleGameResult(score: 10)
        ]
    let resultsAutoId = [
        SimpleGameResultAutoId(score: 1),
        SimpleGameResultAutoId(score: 2),
        SimpleGameResultAutoId(score: 10)
    ]

    var body: some View {
        VStack(alignment: .leading) {
            // identifier is self => integer is unique
            ForEach((1...10).reversed(), id: \.self) {
                Text("\($0)…")
            }

            Text("Ready or not, here I come!")

            // identifier is self => color is unique
            ForEach(colors, id: \.self) { color in
                Text(color.description.capitalized)
                    .padding()
                    .background(color)
            }

            // using custom objects needs unique identifier like this
            ForEach(results, id: \.id) { result in
                Text("Result: \(result.score)")
            }

            // identifier is automatic with Identifiable protocol
            ForEach(resultsAutoId) { result in
                Text("Result: \(result.score)")
            }
        }
    }
}

struct CreateViewsInLoopForEachView_Previews: PreviewProvider {
    static var previews: some View {
        CreateViewsInLoopForEachView()
    }
}
