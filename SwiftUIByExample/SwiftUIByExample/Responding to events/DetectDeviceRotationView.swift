//
//  DetectDeviceRotationView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-device-rotation
//Important: At the time of writing view modifiers do not work with onReceive() unless you first add onAppear(), which is why it appears above. Yes, it’s empty, but it acts as a workaround for the problem.
// Our custom view modifier to track rotation and
// call our action
struct DeviceRotationViewModifier: ViewModifier {
    let action: (UIDeviceOrientation) -> Void

    func body(content: Content) -> some View {
        content
            .onAppear()
            .onReceive(NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)) { _ in
                action(UIDevice.current.orientation)
            }
    }
}

// A View wrapper to make the modifier easier to use
extension View {
    func onRotate(perform action: @escaping (UIDeviceOrientation) -> Void) -> some View {
        self.modifier(DeviceRotationViewModifier(action: action))
    }
}

struct DetectDeviceRotationView: View {
    @State private var orientation = UIDevice.current.orientation

    var body: some View {
        Group {
            if orientation.isPortrait {
                Text("Portrait")
            } else if orientation.isLandscape {
                Text("Landscape")
            } else if orientation.isFlat {
                Text("Flat")
            } else {
                Text("Unknown")
            }
        }
        .onAppear() {
            print("on appear")
            orientation = UIDevice.current.orientation
        }
        .onRotate { newOrientation in
            orientation = newOrientation
        }
    }
}

struct DetectDeviceRotationView_Previews: PreviewProvider {
    static var previews: some View {
        DetectDeviceRotationView()
    }
}
