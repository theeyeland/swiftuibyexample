//
//  KeyboardShortcutsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-keyboard-shortcuts-using-keyboardshortcut
struct KeyboardShortcutsView: View {
    var body: some View {

        VStack {
            Button("Log in") {
                print("Authenticating…")
            }
            .keyboardShortcut("e") // automatic cmd + e
            Divider()
            Button("Run") {
                print("Running…")
            }
            .keyboardShortcut("r", modifiers: .shift) // specified modifier shift + r

            Divider()
            Button("Home") {
                print("Going home")
            }
            .keyboardShortcut("h", modifiers: [.control, .option, .command]) // specified more modifiers
            Divider()
            Button("Confirm Launch") {
                print("Launching drone…")
            }
            .keyboardShortcut(.defaultAction)
        }
    }
}

struct KeyboardShortcutsView_Previews: PreviewProvider {
    static var previews: some View {
        KeyboardShortcutsView()
    }
}
