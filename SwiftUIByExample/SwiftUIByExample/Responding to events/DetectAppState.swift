//
//  DetectAppState.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-when-your-app-moves-to-the-background-or-foreground-with-scenephase
// New in iOS 14
struct DetectAppState: View {
    @Environment(\.scenePhase) var scenePhase

        var body: some View {
            Text("Example Text")
                .onChange(of: scenePhase) { newPhase in
                    if newPhase == .inactive {
                        print("Inactive")
                    } else if newPhase == .active {
                        print("Active")
                    } else if newPhase == .background {
                        print("Background")
                    }
                }
        }
}

struct DetectAppState_Previews: PreviewProvider {
    static var previews: some View {
        DetectAppState()
    }
}
