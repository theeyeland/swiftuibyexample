//
//  ClasicAppDelegate.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import UIKit

public func track(_ message: String, file: String = #file, function: String = #function, line: Int = #line ) {
    print("\(message) called from \(function) \(file):\(line)")
}

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-an-appdelegate-to-a-swiftui-app
// New in iOS 14
// in SwiftUIByExampleApp you need to add UIApplicationDelegateAdaptor and this code will work
class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        track("✅ tract funcion test")
        print("Your code here")
        return true
    }
}
