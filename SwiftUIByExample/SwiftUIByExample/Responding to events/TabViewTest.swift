//
//  TabViewTest.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-control-which-view-is-shown-when-your-app-launches
struct TabViewTest: View {
    var body: some View {
        TabView {
            KeyboardShortcutsView().tabItem {
                Label("Keyboard shorcuts", systemImage: "keyboard")
            }
            OnAppear_DisAppearStateView().tabItem {
                Label("OnAppear - Disapear", systemImage: "eye")
            }
            DetectAppState().tabItem {
                Label("App State", systemImage: "eye.circle")
            }
        }
    }
}

struct TabViewTest_Previews: PreviewProvider {
    static var previews: some View {
        TabViewTest()
    }
}
