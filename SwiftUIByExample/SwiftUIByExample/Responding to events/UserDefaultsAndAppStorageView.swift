//
//  UserDefaultsAndAppStorageView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-run-code-when-your-app-launches
// New in iOS 14
struct UserDefaultsAndAppStorageView: View {
    @AppStorage("name") var name = "Anonymous" // AppStorage needs always a default value

        var body: some View {
            Text("Your name is \(name).")
        }
}

struct UserDefaultsAndAppStorageView_Previews: PreviewProvider {
    static var previews: some View {
        UserDefaultsAndAppStorageView()
    }
}
