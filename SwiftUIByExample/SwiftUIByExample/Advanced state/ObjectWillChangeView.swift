//
//  ObjectWillChangeView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 23/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-send-state-updates-manually-using-objectwillchange
// Create an observable object class that announces
// changes to its only property
class UserAuthentication: ObservableObject {
    var username = "Taylor" {
        willSet {
            objectWillChange.send() // e.g. whe dont have @Published property and we want to notify its @StateObject or @ObservedObject in some View that this object was changed
        }
    }
}

struct ObjectWillChangeView: View {
    // Create an instance of our object
    @StateObject var user = UserAuthentication()

    var body: some View {
        VStack(alignment: .leading) {
            TextField("Enter your name", text: $user.username)
            Text("Your username is: \(user.username)")
        }
    }
}

struct ObjectWillChangeView_Previews: PreviewProvider {
    static var previews: some View {
        ObjectWillChangeView()
    }
}
