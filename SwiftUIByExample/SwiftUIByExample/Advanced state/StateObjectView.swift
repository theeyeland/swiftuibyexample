//
//  StateObjectView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 23/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/whats-the-difference-between-observedobject-state-and-environmentobject
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-stateobject-to-create-and-monitor-external-objects
// An example class to work with
class Player: ObservableObject {
    @Published var name = "Taylor"
    @Published var age = 26
}

struct StateObjectView: View {
    // this class is the owner of Plyer()
    // and StateObject (iOS 14 only)) must be used to create an instance of ObservalbeObject
    @StateObject var player = Player()

        var body: some View {
            NavigationView {
                VStack {
                    NavigationLink(destination: LinkPresenter { PlayerNameView(player: player) } ) {
                        Text("Show detail view")
                    }
                    Text("Hello, \(player.name)!, \(player.age)")
                }
            }
        }
}

// A view that monitors the Player object for changes, but
// doesn't own it.
struct PlayerNameView: View {

    // Here is just a reference to Player this class is not owner of Player
    @ObservedObject var player: Player
    let dealloc: DeallocPrinter

    init(player: Player) {
        self.player = player
        self.dealloc = DeallocPrinter(name: "\(type(of: self))")
    }

    var body: some View {
        Text("Hello, \(player.name)!, \(player.age)")
            .onTapGesture(count: 2) {
                player.name = "Zoltan"
                player.age = 47
        }
    }
}

struct StateObjectView_Previews: PreviewProvider {
    static var previews: some View {
        StateObjectView()
    }
}

// workaround to dealloc detail view
struct LinkPresenter<Content: View>: View {
    let content: () -> Content

    @State private var invlidated = false
    init(@ViewBuilder _ content: @escaping () -> Content) {
        self.content = content
    }
    var body: some View {
        Group {
            if self.invlidated {
                EmptyView()
            } else {
                content()
            }
        }
        .onDisappear { self.invlidated = true }
    }
}

// workaround to check the struct was dealocated
class DeallocPrinter {

    var name: String = "DeallocPrinter"

    init(name: String = "") {
        self.name = name
    }

    deinit {
        print("✅ \(name) was deallocated")
    }
}
