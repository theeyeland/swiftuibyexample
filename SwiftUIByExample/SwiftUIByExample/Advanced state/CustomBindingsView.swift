//
//  CustomBindingsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-custom-bindings
struct CustomBindingsViewTest: View {
    @State private var username = ""

    var body: some View {
        let binding = Binding(
            get: { self.username },
            set: { self.username = $0 }
        )

        return VStack {
            TextField("Enter your name", text: binding)
            Text(username)
        }
    }
}

// Custom binding is great to add extra functinality to binding
struct CustomBindingsView: View {
    @State private var firstToggle = false
    @State private var secondToggle = false

     var body: some View {
         let firstBinding = Binding(
             get: { self.firstToggle },
             set: {
                 self.firstToggle = $0

                 if $0 == true {
                     self.secondToggle = false
                 }
             }
         )

         let secondBinding = Binding(
             get: { self.secondToggle },
             set: {
                 self.secondToggle = $0

                 if $0 == true {
                     self.firstToggle = false
                 }
             }
         )

         return VStack {
             Toggle(isOn: firstBinding) {
                 Text("First toggle")
             }

             Toggle(isOn: secondBinding) {
                 Text("Second toggle")
             }
         }
     }
}

struct CustomBindingsView_Previews: PreviewProvider {
    static var previews: some View {
        CustomBindingsView()
    }
}
