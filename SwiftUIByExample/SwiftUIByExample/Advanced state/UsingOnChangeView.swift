//
//  UsingOnChangeView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-run-some-code-when-state-changes-using-onchange
struct UsingOnChangeView: View {
    @State private var name = ""

        var body: some View {
            TextField("Enter your name:", text: $name)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .onChange(of: name) { newValue in
                    print("Name changed to \(name)!")
                }
        }
}

// Paul Hudson prefered way to use on change
extension Binding {
    func onChange(_ handler: @escaping (Value) -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler(newValue)
            }
        )
    }
}

struct UsingOnChangeView2: View {
    @State private var name = ""

    var body: some View {
        TextField("Enter your name:", text: $name.onChange(nameChanged))
            .textFieldStyle(RoundedBorderTextFieldStyle())
    }

    func nameChanged(to value: String) {
        print("Name changed to \(name)!")
    }
}

struct UsingOnChangeView_Previews: PreviewProvider {
    static var previews: some View {
        UsingOnChangeView()
    }
}
