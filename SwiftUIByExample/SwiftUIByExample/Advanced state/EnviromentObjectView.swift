//
//  EnviromentObjectView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 23/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-environmentobject-to-share-data-between-views
// Our observable object class
class GameSettings: ObservableObject {
    @Published var score = 0
}

// A view that expects to find a GameSettings object
// in the environment, and shows its score.
struct ScoreView: View {
    @EnvironmentObject var settings: GameSettings // SwiftUI automaticly initialized this with settings which was registered as enviromentObject (somehere) - if it was not added to enivroment objcet somewhere it will crash

    var body: some View {
        Text("Score: \(settings.score)")
    }
}

// A view that creates the GameSettings object,
// and places it into the environment for the
// navigation view.
struct EnviromentObjectView: View {
    @StateObject var settings = GameSettings()

    var body: some View {
        NavigationView {
            VStack {
                // A button that writes to the environment settings
                Button("Increase Score") {
                    settings.score += 1
                }

                NavigationLink(destination: ScoreView()) {
                    Text("Show Detail View")
                }
            }
            .frame(height: 200)
        }
        .environmentObject(settings)
    }
}

struct EnviromentObjectView_Previews: PreviewProvider {
    static var previews: some View {
        EnviromentObjectView()
    }
}
