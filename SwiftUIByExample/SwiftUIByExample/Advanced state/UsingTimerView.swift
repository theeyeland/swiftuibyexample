//
//  UsingTimerView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 24/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-a-timer-with-swiftui
struct UsingTimerView: View {
    @State var currentDate = Date()
    @State var timeRemaining = 10
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

    var body: some View {
        VStack {
            Text("\(currentDate)")
                .onReceive(timer) { input in
                    currentDate = input
                }
            Text("Remaining Time: \(timeRemaining)")
                .onReceive(timer) { _ in
                    if timeRemaining > 0 {
                        timeRemaining -= 1
                    }
                }
        }
    }
}

struct UsingTimerView_Previews: PreviewProvider {
    static var previews: some View {
        UsingTimerView()
    }
}
