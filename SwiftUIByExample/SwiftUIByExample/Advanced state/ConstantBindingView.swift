//
//  ConstantBindingView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 23/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-constant-bindings
struct ConstantBindingView: View {
    var body: some View {
        Toggle(isOn: .constant(true)) {
            Text("Show advanced options")
        }
    }
}

struct ConstantBindingView_Previews: PreviewProvider {
    static var previews: some View {
        ConstantBindingView()
    }
}
