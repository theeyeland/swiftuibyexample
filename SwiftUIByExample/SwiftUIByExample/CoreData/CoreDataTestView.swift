//
//  CoreDataTestView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI
import CoreData

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-filter-core-data-fetch-requests-using-a-predicate
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-core-data-objects-from-swiftui-views
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-delete-core-data-objects-from-swiftui-views
//https://www.hackingwithswift.com/quick-start/swiftui/how-to-limit-the-number-of-items-in-a-fetch-request
struct CoreDataTestView: View {

    @Environment(\.managedObjectContext) var managedObjectContext

    @FetchRequest(
        entity: ProgrammingLanguage.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \ProgrammingLanguage.name, ascending: true),
            NSSortDescriptor(keyPath: \ProgrammingLanguage.creator, ascending: false)
        ]
    ) var languages: FetchedResults<ProgrammingLanguage>

    // example of FetchRequest with predicate
    @FetchRequest(
        entity: ProgrammingLanguage.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \ProgrammingLanguage.name, ascending: true),
        ],
        predicate: NSPredicate(format: "name == %@", "Python")
    ) var languagesWithPredicate: FetchedResults<ProgrammingLanguage>

    // example of FetchRequest with with Limit
    // must be done with initializer
    @FetchRequest var languagesWithLimit: FetchedResults<ProgrammingLanguage>

    init() {
        let request: NSFetchRequest<ProgrammingLanguage> = ProgrammingLanguage.fetchRequest()

        request.sortDescriptors = [
            NSSortDescriptor(keyPath: \ProgrammingLanguage.name, ascending: true)
        ]

        request.fetchLimit = 10
        _languagesWithLimit = FetchRequest(fetchRequest: request)
    }

    var body: some View {
        NavigationView {
            List {
                ForEach(languages, id: \.self) { language in
                    Text("Creator: \(language.creator ?? "Anonymous")")
                }
                .onDelete(perform: removeLanguages)
            }

            .navigationTitle("Programming Languages")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    EditButton()
                }

                ToolbarItemGroup(placement: .navigationBarLeading) {

                    Button(action: {
                        print("Edit button was tapped")
                        let language = ProgrammingLanguage(context: managedObjectContext)
                        language.name = "Python"
                        language.creator = "Guido van Rossum"
                        // more code here

                        PersistenceController.shared.save()
                    }) {
                        Image(systemName: "pencil")
                    }
                }
            }
        }
    }
    

    func removeLanguages(at offsets: IndexSet) {
        for index in offsets {
            let language = languages[index]
            managedObjectContext.delete(language)
        }
        PersistenceController.shared.save()
    }
}

struct CoreDataTestView_Previews: PreviewProvider {
    static var previews: some View {
        CoreDataTestView()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
