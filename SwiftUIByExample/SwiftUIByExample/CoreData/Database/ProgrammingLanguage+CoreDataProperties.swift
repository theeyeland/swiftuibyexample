//
//  ProgrammingLanguage+CoreDataProperties.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//
//

import Foundation
import CoreData


extension ProgrammingLanguage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProgrammingLanguage> {
        return NSFetchRequest<ProgrammingLanguage>(entityName: "ProgrammingLanguage")
    }

    @NSManaged public var name: String?
    @NSManaged public var creator: String?

}

extension ProgrammingLanguage : Identifiable {

}
