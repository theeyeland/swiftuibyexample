//
//  MarkContentAsPlaceholder.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-mark-content-as-a-placeholder-using-redacted
// redacted is cool when content is loading
// New in iOS 14
struct MarkContentAsPlaceholder: View {

    @Environment(\.redactionReasons) var redactionReasons
    let bio = "The rain in Spain falls mainly on the Spaniards"

    var body: some View {
        VStack {
            Text("This is placeholder text")
                .padding()
                .font(.title)
                .redacted(reason: .placeholder)

            VStack {
                Text("This is placeholder text")
                Text("And so is this")
            }
            .font(.title)
            .redacted(reason: .placeholder)

            if redactionReasons == .placeholder {
                Text("Loading…")
            } else {
                Text(bio)
                    .redacted(reason: redactionReasons)
            }
        }
    }
}

struct MarkContentAsPlaceholder_Previews: PreviewProvider {
    static var previews: some View {
        MarkContentAsPlaceholder()
    }
}
