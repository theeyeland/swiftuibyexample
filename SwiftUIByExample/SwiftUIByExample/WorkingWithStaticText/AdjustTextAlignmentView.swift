//
//  ContentView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-adjust-text-alignment-using-multilinetextalignment
struct AdjustTextAlignmentView: View {

    let alignments: [TextAlignment] = [.leading, .center, .trailing]

    @State private var alignment = TextAlignment.leading

    var body: some View {

        VStack {
            Picker("Text alignment", selection: $alignment) {
                ForEach(alignments, id: \.self) { alignment in
                    Text(String(describing: alignment))
                        .font(.title)
                        .foregroundColor(Color.green)
                }
            }

            Text("This is some longer text that is limited to three lines maximum, so anything more than that will cause the text to clip.")
                .fontWeight(.thin)
                .padding(10.0)
                .font(.largeTitle)
                .frame(width: 330.0)
                .background(Color.yellow)
                .foregroundColor(Color.blue)
                .multilineTextAlignment(alignment)
                .lineLimit(nil)
                .truncationMode(.middle)

        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AdjustTextAlignmentView()
    }
}
