//
//  AddSpacingBetweenLettersView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-spacing-between-letters-in-text
struct AddSpacingBetweenLettersView: View {

    @State private var amount: CGFloat = 50

        var body: some View {
            VStack {

                Text("Hello World")
                    .tracking(25)
                    .padding()

                Text("ffi")
                    .font(.custom("AmericanTypewriter", size: 72))
                    .kerning(amount)
                Text("ffi")
                    .font(.custom("AmericanTypewriter", size: 72))
                    .tracking(amount)

                Slider(value: $amount, in: 0...100) {
                    Text("Adjust the amount of spacing")
                }
            }
        }
}

struct AddSpacingBetweenLettersView_Previews: PreviewProvider {
    static var previews: some View {
        AddSpacingBetweenLettersView()
    }
}
