//
//  StaticLabelsWithTextView-.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-static-labels-with-a-text-view
struct StaticLabelsWithTextViewView: View {
    var body: some View {

        VStack {
            Text("This is some longer text that is limited to three lines maximum, so anything more than that will cause the text to clip.")
                .padding()
                .lineLimit(3)
//                .lineLimit(nil)
                .frame(width: 200)


            Text("This is some longer text that is limited to three lines maximum, so anything more than that will cause the text to clip.")
                .padding()
                .lineLimit(1)
                .truncationMode(.middle)

        }
    }
}

struct StaticLabelsWithTextView__Previews: PreviewProvider {
    static var previews: some View {
        StaticLabelsWithTextViewView()
    }
}
