//
//  StyleTextWithFontsColorsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-style-text-views-with-fonts-colors-line-spacing-and-more
struct StyleTextWithFontsColorsView: View {
    var body: some View {
        VStack() {
            Text("This is an extremely long text string that will never fit even the widest of phones without wrapping")
                .padding()
                .font(.largeTitle)
                .foregroundColor(Color.secondary)
                .frame(width: 300)

            Text("The best laid plans")
                .padding()
                .foregroundColor(Color.red)

            Text("The best laid plans")
                .padding()
                .background(Color.red)
                .foregroundColor(.white)
                .font(.headline)

            Text("This is an extremely long text string that will never fit even the widest of phones without wrapping")
                .padding()
                .font(.largeTitle)
                .lineSpacing(25)
                .frame(width: 300)
        }
    }
}

struct StyleTextWithFontsColorsView_Previews: PreviewProvider {
    static var previews: some View {
        StyleTextWithFontsColorsView()
    }
}
