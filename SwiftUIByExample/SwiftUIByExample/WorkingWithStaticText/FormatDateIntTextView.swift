//
//  FormatDateIntTextView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-format-dates-inside-text-views
// New in iOS 14
struct FormatDateIntTextView: View {
    var body: some View {
        VStack {
            // Date range
            Text(Date()...Date().addingTimeInterval(600))
                .padding()

            // show just the date
            Text(Date().addingTimeInterval(600), style: .date)
                .padding()

            // show just the time
            Text(Date().addingTimeInterval(600), style: .time)
                .padding()

            // show the relative distance from now, automatically updating
            Text(Date().addingTimeInterval(600), style: .relative)
                .padding()

            // make a timer style, automatically updating
            Text(Date().addingTimeInterval(600), style: .timer)
                .padding()
        }
    }
}

struct FormatDateIntTextView_Previews: PreviewProvider {
    static var previews: some View {
        FormatDateIntTextView()
    }
}
