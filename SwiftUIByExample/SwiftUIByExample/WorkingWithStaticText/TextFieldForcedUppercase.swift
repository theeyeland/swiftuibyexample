//
//  TextFieldForcedUppercase.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-textfield-uppercase-or-lowercase-using-textcase
// New in iOS 14
struct TextFieldForcedUppercase: View {
    @State private var name = "Paul"

    var body: some View {
        TextField("Shout your name at me", text: $name)
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .textCase(.uppercase)
            .padding(.horizontal)
    }
}

struct TextFieldForcedUppercase_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldForcedUppercase()
    }
}
