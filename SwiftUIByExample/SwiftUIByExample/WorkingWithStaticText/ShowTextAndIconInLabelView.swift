//
//  ShowTextAndIconInLabelView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-text-and-an-icon-side-by-side-using-label
// New in iOS 14
struct ShowTextAndIconInLabelView: View {
    var body: some View {
        VStack(alignment: .leading) {

            Label("Your account", systemImage: "person.crop.circle")
                .padding()

            Label("Text Only", systemImage: "heart")
                .font(.title)
                .labelStyle(TitleOnlyLabelStyle())

            Label("Icon Only", systemImage: "star")
                .font(.title)
                .labelStyle(IconOnlyLabelStyle())

            Label("Both", systemImage: "paperplane")
                .font(.title)
//                .labelStyle(TitleAndIconLabelStyle()) // available in ios 14.5 XCode 12.5 - but it is default behaviour


            Label {
                Text("Paul Hudson")
                    .foregroundColor(.primary)
                    .font(.largeTitle)
                    .padding()
                    .background(Color.gray.opacity(0.2))
                    .clipShape(Capsule())
            } icon: {
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.blue)
                    .frame(width: 64, height: 64)
            }
        }
    }
}

struct ShowTextAndIconInLabelView_Previews: PreviewProvider {
    static var previews: some View {
        ShowTextAndIconInLabelView()
    }
}
