//
//  FormatTextInTextView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-format-text-inside-text-views
struct FormatTextInTextView: View {

    static let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()

    let dueDate = Date()

    var body: some View {
        Text("Task due date: \(dueDate, formatter: Self.taskDateFormat)")
    }
}

struct FormatTextInTextView_Previews: PreviewProvider {
    static var previews: some View {
        FormatTextInTextView()
    }
}
