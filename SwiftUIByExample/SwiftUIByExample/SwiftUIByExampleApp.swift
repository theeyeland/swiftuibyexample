//
//  SwiftUIByExampleApp.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI
import CoreSpotlight

@main
struct SwiftUIByExampleApp: App {

    let persistenceController = PersistenceController.shared
    @Environment(\.scenePhase) var scenePhase

    // https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-an-appdelegate-to-a-swiftui-app
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    // https://www.hackingwithswift.com/quick-start/swiftui/how-to-run-code-when-your-app-launches
    // register initial UserDefaults values every launch
    init() {
        UserDefaults.standard.register(defaults: [
            "name": "Taylor Swift",
            "highScore": 10
        ])
    }

    // https://www.hackingwithswift.com/quick-start/swiftui/how-to-continue-an-nsuseractivity-in-swiftui
    func handleSpotlight(_ userActivity: NSUserActivity) {
        if let id = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String {
            print("Found identifier \(id)")
        }
    }

    var body: some Scene {
        // uncomment for document base app
//        DocumentGroup(newDocument: TextFile()) { file in
//            DocumentBased(document: file.$document)
//        }
        WindowGroup {
            BottomSheet()
//            CoreDataTestView()
//                .environment(\.managedObjectContext, persistenceController.container.viewContext)
//                .onContinueUserActivity(CSSearchableItemActionType, perform: handleSpotlight)
        }
        .onChange(of: scenePhase) { _ in
            persistenceController.save()
        }
    }
}
