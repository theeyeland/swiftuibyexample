//
//  UsingPickerForMenuView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-let-users-pick-options-from-a-menu
struct UsingPickerForMenuView: View {
    @State private var selection = "Red"
    let colors = ["Red", "Green", "Blue", "Black", "Orange"]
    let realColors: [String: Color] = ["Red": Color.red, "Green": .green, "Blue": .blue, "Black": .black, "Orange": .orange]

    var body: some View {
        VStack {
            Picker("Select a paint color", selection: $selection) {
                ForEach(colors, id: \.self) {
                    Text($0)
                }
            }
            .pickerStyle(MenuPickerStyle())

            Text("Selected color: \(selection)")
                .foregroundColor(realColors[selection, default: .purple])
        }
    }
}

struct UsingPickerForMenuView_Previews: PreviewProvider {
    static var previews: some View {
        UsingPickerForMenuView()
    }
}
