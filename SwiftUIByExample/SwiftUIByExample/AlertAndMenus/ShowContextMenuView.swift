//
//  ShowContextMenuView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-a-context-menu
// show context menu with logn press
// can be added to any view
struct ShowContextMenuView: View {
    var body: some View {
        Text("Options")
            .contextMenu {
                Button {
                    print("Change country setting")
                } label: {
                    Label("Choose Country", systemImage: "globe")
                }

                Button {
                    print("Enable geolocation")
                } label: {
                    Label("Detect Location", systemImage: "location.circle")
                }
            }
    }
}

struct ShowContextMenuView_Previews: PreviewProvider {
    static var previews: some View {
        ShowContextMenuView()
    }
}
