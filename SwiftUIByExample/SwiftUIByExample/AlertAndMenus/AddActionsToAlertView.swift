//
//  AddActionsToAlertView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-actions-to-alert-buttons
struct AddActionsToAlertView: View {
    @State private var showingAlert = false

    var body: some View {
        Button("Show Alert") {
            showingAlert = true
        }
        .alert(isPresented:$showingAlert) {
            Alert(
                title: Text("Are you sure you want to delete this?"),
                message: Text("There is no undo"),
                primaryButton: .destructive(Text("Delete")) {
                    print("Deleting...")
                },
                secondaryButton: .cancel() {
                    print("Canceled")
                }
            )
        }
    }
}

struct AddActionsToAlertView_Previews: PreviewProvider {
    static var previews: some View {
        AddActionsToAlertView()
    }
}
