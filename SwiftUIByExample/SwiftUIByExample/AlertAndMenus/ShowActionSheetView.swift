//
//  ShowActionSheetView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-an-action-sheet
//it works similarly to alerts.
struct ShowActionSheetView: View {
    @State private var showingSheet = false

    var body: some View {
        Button("Show Action Sheet") {
            showingSheet = true
        }
        .actionSheet(isPresented: $showingSheet) {
            ActionSheet(
                title: Text("What do you want to do?"),
                message: Text("There's only one choice..."),
                buttons: [.default(Text("Dismiss Action Sheet"), action: defaultAction )]
            )
        }
    }

    func defaultAction() {
        print("canceled")
    }
}

struct ShowActionSheetView_Previews: PreviewProvider {
    static var previews: some View {
        ShowActionSheetView()
    }
}
