//
//  HowToShowAlertView2.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-an-alert

//The second approach to creating alerts is to bind to some optional state that conforms to Identifiable, which will cause the alert to be shown whenever the object’s value changes.
//
//There are two advantages to this method:
//
//1. You can attach any object you like at runtime, so your alert can show any number of different pieces of data.
//2. SwiftUI automatically unwraps the optional when it has value, so you can be sure it exists by the time you want to show your alert – no need to check and unwrap the value yourself.
//For example, this shows one alert with two different values depending on which TV show was chosen:

struct TVShow: Identifiable {
    var id: String { name }
    let name: String
}

struct HowToShowAlertView2: View {
    @State private var selectedShow: TVShow? // note that this is optional

        var body: some View {
            VStack(spacing: 20) {
                Text("What is your favorite TV show?")
                    .font(.headline)

                Button("Select Ted Lasso") {
                    selectedShow = TVShow(name: "Ted Lasso")
                }

                Button("Select Bridgerton") {
                    selectedShow = TVShow(name: "Bridgerton")
                }
            }
            .alert(item: $selectedShow) { show in
                Alert(title: Text(show.name), message: Text("Great choice!"), dismissButton: .cancel())
            }
        }
}

struct HowToShowAlertView2_Previews: PreviewProvider {
    static var previews: some View {
        HowToShowAlertView2()
    }
}
