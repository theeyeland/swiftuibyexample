//
//  MultipleAlertsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-multiple-alerts-in-a-single-view
// !!!Remeber: each view can have only one .alert attached
// good habit is attach .alert to e.g. button which is reposnsible to show it,
// but alert can be attached to any view
//If you try moving both alert() modifiers to the VStack, you’ll find that only one works, which is why the above approach is so useful.
struct MultipleAlertsView: View {
    @State private var showingAlert1 = false
    @State private var showingAlert2 = false

    var body: some View {
        VStack {
            Button("Show 1") {
                showingAlert1 = true
            }
            .alert(isPresented: $showingAlert1) {
                Alert(title: Text("One"), message: nil, dismissButton: .cancel())
            }

            Button("Show 2") {
                showingAlert2 = true
            }
            .alert(isPresented: $showingAlert2) {
                Alert(title: Text("Two"), message: nil, dismissButton: .cancel())
            }
        }
    }
}

struct MultipleAlertsView_Previews: PreviewProvider {
    static var previews: some View {
        MultipleAlertsView()
    }
}
