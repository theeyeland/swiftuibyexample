//
//  ShowMenuWithButtonView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-a-menu-when-a-button-is-pressed
// NEW in iOS 14
struct ShowMenuWithButtonView: View {
    var body: some View {
        Menu/*("Options")*/ {
            Button("Order Now", action: placeOrder)
            Button("Adjust Order", action: adjustOrder)
            Menu("Advanced") { // nested menu
                Button("Rename", action: rename)
                Button("Delay", action: delay)
            }
            Button(action: cancelOrder, label : {
                Text("Cancel")
                Image(systemName: "cloud.heavyrain.fill")
            })
        } label: {
            Label("Options", systemImage: "paperplane") // or you can use custum label
        }
    }

    func placeOrder() { }
    func adjustOrder() { }
    func rename() { }
    func delay() { }
    func cancelOrder() { print("cancel order") }
}

struct ShowMenuWithButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ShowMenuWithButtonView()
    }
}
