//
//  AppStoreOverlayView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI
import StoreKit

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-recommend-another-app-using-appstoreoverlay
// NEW in iOS 14
struct AppStoreOverlayView: View {
    @State private var showRecommended = false

    var body: some View {
        Button("Show Recommended App") {
            showRecommended.toggle()
        }
        .appStoreOverlay(isPresented: $showRecommended) {
            SKOverlay.AppConfiguration(appIdentifier: "1440611372", position: .bottom)
        }
    }
}

struct AppStoreOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        AppStoreOverlayView()
    }
}
