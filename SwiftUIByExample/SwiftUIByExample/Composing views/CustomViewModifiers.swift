//
//  CustomViewModifiers.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-custom-modifiers
struct PrimaryLabel: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(Color.red)
            .foregroundColor(Color.white)
            .font(.largeTitle)
    }
}

extension View {
    func primaryLabel() -> some View {
        self.modifier(PrimaryLabel())
    }
}

struct CustomViewModifiers: View {
    var body: some View {
        VStack {
            Text("Hello, SwiftUI")
                .modifier(PrimaryLabel()) // using cutom modifier directly

            Text("Hello, SwiftUI")
                .primaryLabel() // use custom modifier with extension on View
        }
    }
}

struct CustomViewModifiers_Previews: PreviewProvider {
    static var previews: some View {
        CustomViewModifiers()
    }
}
