//
//  CombineTextViewsTogether.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-combine-text-views-together
// this technique is close to NSAttributedString - swiftUI don't have NSAttributedString
struct CombineTextViewsTogether: View {
    var body: some View {
        VStack {
            //            Group {
            (Text("SwiftUI ")
                .font(.largeTitle)
                + Text("is ")
                .font(.headline)
                + Text("awesome")
                .font(.footnote))
                //            }
                .padding() // you can use group or put it to () to add padding

            Text("SwiftUI ")
                .foregroundColor(.red)
            + Text("is ")
                .foregroundColor(.orange)
                .fontWeight(.black)
            + Text("awesome")
                .foregroundColor(.blue)
        }
    }
}

struct CombineTextViewsTogether_Previews: PreviewProvider {
    static var previews: some View {
        CombineTextViewsTogether()
    }
}
