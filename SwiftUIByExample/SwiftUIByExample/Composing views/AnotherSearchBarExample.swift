//
//  AnotherSearchBarExample.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.albertomoral.com/blog/uisearchbar-and-swiftui
struct SearchBar: UIViewRepresentable {

    @Binding var text: String

    class Coordinator: NSObject, UISearchBarDelegate {
        @Binding var text: String

        init(text: Binding<String>) {
            _text = text
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            UIApplication.shared.endEditing()
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            print("cancel clicked")
        }
    }

    func makeCoordinator() -> SearchBar.Coordinator {
        return Coordinator(text: $text)
    }

    func makeUIView(context: Context) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.autocapitalizationType = .none
        searchBar.showsCancelButton = true
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar, context: Context) {
        uiView.text = text
    }
}

extension String {
    func containsCaseInsensitive(_ string: String) -> Bool {
        return self.localizedCaseInsensitiveContains(string)
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

struct GreatUser: Hashable {
//    let id = UUID()
    let name: String
}


struct AnotherSearchBarExample: View {
    private let users = [GreatUser(name: "Alberto"), GreatUser(name: "Diana"), GreatUser(name: "Laura"), GreatUser(name: "Sergio"),
                         GreatUser(name: "Alco"), GreatUser(name: "Puente"), GreatUser(name: "Joan"), GreatUser(name: "Angel")]

    @State var searchText: String = ""

    var body: some View {
        VStack {
            SearchBar(text: $searchText.animation())
            List {
                ForEach(getSearchByName(), id: \.self) { user in
                    Text(user.name)
                }
            }
        }
    }

    private func getSearchByName() -> [GreatUser] {
        guard !searchText.isEmpty else { return users }
        return users.filter { $0.name.containsCaseInsensitive(searchText) }
    }
}

struct AnotherSearchBarExample_Previews: PreviewProvider {
    static var previews: some View {
        AnotherSearchBarExample()
    }
}
