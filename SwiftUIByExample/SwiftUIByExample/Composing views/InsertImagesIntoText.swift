//
//  InsertImagesIntoText.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-insert-images-into-text
struct InsertImagesIntoText: View {
    var body: some View {
        VStack {
            Text("Hello ") + Text(Image(systemName: "star")) + Text(" World!")
            Text("Hello ") + Text(Image(systemName: "star")) + Text(" World!")
                .foregroundColor(.blue)
                .font(.largeTitle)
            (Text("Hello ") + Text(Image(systemName: "star")) + Text(" World!"))
                .foregroundColor(.blue)
                .font(.largeTitle)
        }
    }
}

struct InsertImagesIntoText_Previews: PreviewProvider {
    static var previews: some View {
        InsertImagesIntoText()
    }
}
