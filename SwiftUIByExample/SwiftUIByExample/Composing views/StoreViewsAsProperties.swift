//
//  StoreViewsAsProperties.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-store-views-as-properties
struct StoreViewsAsProperties: View {
    let title = Text("Paul Hudson")
        .bold()
    let subtitle = Text("Author")
        .foregroundColor(.secondary)

    var body: some View {
        VStack {
            title
                .foregroundColor(.red)
            subtitle
        }
    }
}

struct StoreViewsAsProperties_Previews: PreviewProvider {
    static var previews: some View {
        StoreViewsAsProperties()
    }
}
