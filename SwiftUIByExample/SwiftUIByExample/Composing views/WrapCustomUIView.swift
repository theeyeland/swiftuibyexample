//
//  WrapCustomUIView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 28/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-wrap-a-custom-uiview-for-swiftui

struct TextView: UIViewRepresentable {
    @Binding var text: NSMutableAttributedString

    // This was added by Me for test purpose
    func makeCoordinator() -> TextViewCoordinator {
      return TextViewCoordinator(text: $text)
    }

    class TextViewCoordinator: NSObject, UITextViewDelegate {
        @Binding var text: NSMutableAttributedString

        init(text: Binding<NSMutableAttributedString>) {
            _text = text
        }

        func textViewDidBeginEditing(_ textView: UITextView) {
            print(textView.attributedText!)
        }

        func textViewDidChange(_ textView: UITextView) {
            text =  NSMutableAttributedString(attributedString: textView.attributedText)
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

            print(text)
            return true
        }

    }

    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.delegate = context.coordinator
        return textView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.attributedText = text
    }
}

struct WrapCustomUIView: View {
    @State var text = NSMutableAttributedString(string: "Wraped custum UITextView")

     var body: some View {
        VStack {
            TextView(text: $text)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)

            Text(text.string)
        }
     }
}

struct WrapCustomUIView_Previews: PreviewProvider {
    static var previews: some View {
        WrapCustomUIView()
    }
}
