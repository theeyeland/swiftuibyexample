//
//  GroupViewsTogether.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-group-views-together
struct GroupViewsTogether: View {
    var body: some View {
        VStack {
            VStack {
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                //Text("Line") // Swift UI limits views to max 10 - workaround is using Groups - see below
            }

            VStack {
                Group {
                    Text("Line")
                    Text("Line")
                    Text("Line")
                    Text("Line")
                    Text("Line")
                    Text("Line")
                }

                Group {
                    Text("Line")
                    Text("Line")
                    Text("Line")
                    Text("Line")
                    Text("Line")
                }
            }
        }
    }
}

struct GroupViewsTogether_Previews: PreviewProvider {
    static var previews: some View {
        GroupViewsTogether()
    }
}
