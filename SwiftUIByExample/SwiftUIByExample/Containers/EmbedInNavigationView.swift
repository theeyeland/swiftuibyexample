//
//  EmbedInNavigationView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-embed-a-view-in-a-navigation-view
struct EmbedInNavigationView: View {
    var body: some View {
        NavigationView {
            Text("SwiftUI")
                .navigationTitle("Welcome")
                .navigationBarTitleDisplayMode(.inline) // defualt is Large Title
        }
    }
}

struct EmbedInNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        EmbedInNavigationView()
    }
}
