//
//  UsingTabView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-embed-views-in-a-tab-bar-using-tabview
struct UsingTabView: View {
    var body: some View {
        TabView {
            Text("First View")
                .padding()
                .tabItem {
                    Image(systemName: "1.circle")
                    Text("First")
//                    Label("First", systemImage: "1.circle") // or you can use label
                }
                .tag(1) // add tag for programatic control
            Text("Second View")
                .padding()
                .tabItem {
//                    Image(systemName: "2.circle")
//                    Text("Second")
                    Label("Second", systemImage: "2.circle") // or you can use label
                }
                .tag(2)
        }
    }
}

// change active tab with tab view
struct UsingTabView2: View {
    @State var selectedView = 1

    var body: some View {
        TabView(selection: $selectedView) {
            Button("Show Second View") {
                selectedView = 2
            }
            .padding()
            .tabItem {
                Label("First", systemImage: "1.circle")
            }
            .tag(1)

            Button("Show First View") {
                selectedView = 1
            }
            .padding()
            .tabItem {
                Label("Second", systemImage: "2.circle")
            }
            .tag(2)
        }
    }
}

struct UsingTabView_Previews: PreviewProvider {
    static var previews: some View {
        UsingTabView()
    }
}
