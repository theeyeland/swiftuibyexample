//
//  AddSideBarForiPadOSView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

//https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-a-sidebar-for-ipados
// NEW in iOS 14
struct AddSideBarForiPadOSView: View {
    var body: some View {
        NavigationView {
//            Text("Sidebar")
            List(1..<100) { i in
                Text("Row \(i)")
            }
            .listStyle(SidebarListStyle())
            Text("Primary View")
            Text("Detail View")
        }
    }
}

struct AddSideBarForiPadOSView_Previews: PreviewProvider {
    static var previews: some View {
        AddSideBarForiPadOSView()
    }
}
