//
//  ScrollingPagesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-scrolling-pages-of-content-using-tabviewstyle
// NEW in iOS 14
struct ScrollingPagesView: View {
    var body: some View {
        TabView {
            Text("First")
            Text("Second")
            Text("Third")
            Text("Fourth")
        }
        .background(Color.blue)
//        .ignoresSafeArea()
        .tabViewStyle(PageTabViewStyle())

//        .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always)) // diffrent page dots
//        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never)) // hide page dots alwayes
    }
}

struct ScrollingPagesView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollingPagesView()
    }
}
