//
//  ToolbarItemsInNavigationView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-bar-items-to-a-navigation-view
struct ToolbarItemsInNavigationView: View {
    var body: some View {
//        NavigationView {
//            Text("SwiftUI")
//                .navigationTitle("Welcome")
//                .toolbar { // here is placement automatic system desides according left-right languages
//                    Button("Help") {
//                        print("Help tapped!")
//                    }
//                }
//        }
//
//        NavigationView {
//            Text("SwiftUI")
//                .navigationTitle("Welcome")
//                .toolbar {
//                    ToolbarItem(placement: .navigationBarLeading) { // tell exact placement
//                        Button("Help") {
//                            print("Help tapped!")
//                        }
//                    }
//                }
//        }

        NavigationView {
            Text("SwiftUI")
                .navigationTitle("Welcome")
                .toolbar {
                    ToolbarItemGroup(placement: .navigationBarTrailing) {
                        Button("About") {
                            print("About tapped!")
                        }

                        Button("Help") {
                            print("Help tapped!")
                        }

                        Button(action: {
                            print("Edit button was tapped")
                        }) {
                            Image(systemName: "pencil")
                        }
                    }
                }
        }.accentColor(Color.purple)
    }
}

struct ToolbarItemsInNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        ToolbarItemsInNavigationView()
    }
}
