//
//  RevealContentDisclosureGroupView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

//https://www.hackingwithswift.com/quick-start/swiftui/how-to-hide-and-reveal-content-using-disclosuregroup
struct RevealContentDisclosureGroupView: View {
    @State private var revealDetails = false

    var body: some View {
        // use isExpanded to control the state programaticly, but it works without it
        DisclosureGroup("Show Terms", isExpanded: $revealDetails) {
            Text("Long terms and conditions here long terms and conditions here long terms and conditions here long terms and conditions here long terms and conditions here long terms and conditions here.")
        }
        .accentColor(.yellow)
        .frame(width: 300)
        // to add posibility to reveal by tap on text alse, otherwise it works only with tap on disclosure icon
        .onTapGesture {
            withAnimation {
                revealDetails = !revealDetails
            }
        }
    }
}

struct RevealContentDisclosureGroupView_Previews: PreviewProvider {
    static var previews: some View {
        RevealContentDisclosureGroupView()
    }
}
