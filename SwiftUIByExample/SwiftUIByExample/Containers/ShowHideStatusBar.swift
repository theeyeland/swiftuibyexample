//
//  ShowHideStatusBar.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-hide-and-show-the-status-bar
struct ShowHideStatusBar: View {
    @State private var hideStatusBar = false

    var body: some View {
        Button("Toggle Status Bar") {
            withAnimation {
                hideStatusBar.toggle()
            }
        }
        .statusBar(hidden: hideStatusBar)
    }
}

struct ShowHideStatusBar_Previews: PreviewProvider {
    static var previews: some View {
        ShowHideStatusBar()
    }
}
