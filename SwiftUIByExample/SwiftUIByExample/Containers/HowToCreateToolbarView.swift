//
//  HowToCreateToolbarView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 25/05/2021.
//

import SwiftUI

struct HowToCreateToolbarView: View {

    init() {
        UIToolbar.appearance().barTintColor = UIColor.blue
    }

    var body: some View {
        NavigationView {
            Text("Hello, World!").padding()
                .navigationTitle("SwiftUI")
                .toolbar {
                    // this is just for one button - use ToolbarItemGroup for more buttons
//                    ToolbarItem(placement: .bottomBar) {
//                        Button("Press Me") {
//                            print("Pressed")
//                        }
//                    }

                    ToolbarItemGroup(placement: .bottomBar) {
                        Button("First") {
                            print("Pressed")
                        }

                        Spacer()

                        Button("Second") {
                            print("Pressed")
                        }

                        Spacer()

                        Button(action: {
                            print("Edit button was tapped")
                        }) {
                            Image(systemName: "pencil")
                        }
                    }
                }
        }.accentColor(.white)
    }
}

struct HowToCreateToolbarView_Previews: PreviewProvider {
    static var previews: some View {
        HowToCreateToolbarView()
    }
}
