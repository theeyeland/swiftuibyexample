//
//  CustomizeStackLayouts.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-customize-stack-layouts-with-alignment-and-spacing
struct CustomizeStackLayouts: View {
    var body: some View {
        VStack {
            VStack(alignment: .center, spacing: 0) {
                Text("SwiftUI")
                Divider()
                Text("rocks")
            }
            Divider().background(Color.blue)
            HStack(alignment: .center, spacing: 0) {
                Text("SwiftUI is cool")
                    .frame(width: .infinity)
                    .padding(.leading, 8)
                Spacer()
                Text("rocks")
                    .frame(width: .infinity)
                    .padding(.trailing, 8)

            }
//            .fixedSize(horizontal: false, vertical: true)
            Divider()
            Spacer()
        }
    }
}

struct CustomizeStackLayouts_Previews: PreviewProvider {
    static var previews: some View {
        CustomizeStackLayouts()
    }
}
