//
//  LazyVGridView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 17/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-position-views-in-a-grid-using-lazyvgrid-and-lazyhgrid
//New in iOS 14
struct LazyVGridView: View {
    let data = (1...1000).map { "Item \($0)" }

    // how many collums you want in the row
        let columns = [
//            GridItem(.adaptive(minimum: 80)),
            GridItem(.fixed(80)),
            GridItem(.flexible()),
            GridItem(.flexible()),
            GridItem(.flexible())
//            GridItem(.flexible()),
//            GridItem(.flexible())
//            GridItem(.flexible())
        ]

        var body: some View {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 10) {
                    ForEach(data, id: \.self) { item in
                        Text(item)
                            .padding()
                            .background(Color.yellow)
                            .cornerRadius(/*@START_MENU_TOKEN@*/8.0/*@END_MENU_TOKEN@*/)
                    }
                }
                .padding(.horizontal)
            }
//            .frame(maxHeight: 300)
        }
}

struct LazyVGridView_Previews: PreviewProvider {
    static var previews: some View {
        LazyVGridView()
    }
}
