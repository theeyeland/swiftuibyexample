//
//  FixedSizeSpacerView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-a-fixed-size-spacer
struct FixedSizeSpacerView: View {
    var body: some View {
        VStack {
            Text("First Label")
            Spacer()
                .frame(height: 50)
//                .frame(minHeight: 50, maxHeight: 1000)
            Text("Second Label")
        }
        HStack {
            Text("First Label")
            Spacer()
                .frame(width: 50)
            Text("Second Label")
        }
    }
}

struct FixedSizeSpacerView_Previews: PreviewProvider {
    static var previews: some View {
        FixedSizeSpacerView()
    }
}
