//
//  LazyHGridView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-position-views-in-a-grid-using-lazyvgrid-and-lazyhgrid
//New in iOS 14
struct LazyHGridView: View {
    let items = 1...50

        let rows = [
            GridItem(.fixed(50)),
            GridItem(.fixed(50))
        ]

        var body: some View {
            ScrollView(.horizontal) {
                LazyHGrid(rows: rows, alignment: .center) {
                    ForEach(items, id: \.self) { item in
                        Image(systemName: "\(item).circle.fill")
                            .font(.largeTitle)
                    }
                }
                .frame(height: 150)
            }
        }
}

struct LazyHGridView_Previews: PreviewProvider {
    static var previews: some View {
        LazyHGridView()
    }
}
