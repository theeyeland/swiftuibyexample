//
//  ZStackWithZIndexView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-change-the-order-of-view-layering-using-z-index
struct ZStackWithZIndexView: View {
    var body: some View {

        // zIndex defines order of objects default is 0
        // it is aplied only in current ZStack
        ZStack(alignment: .bottomTrailing) {
            Rectangle()
                .fill(Color.green)
                .frame(width: 50, height: 50)
                .zIndex(1)
                .offset(x:-5, y:-5)

            Rectangle()
                .fill(Color.red)
                .frame(width: 100, height: 100)
        }
    }
}

struct ZStackWithZIndexView_Previews: PreviewProvider {
    static var previews: some View {
        ZStackWithZIndexView()
    }
}
