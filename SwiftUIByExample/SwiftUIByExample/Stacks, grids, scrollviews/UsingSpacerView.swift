//
//  UsingSpacerView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-force-views-to-one-side-inside-a-stack-using-spacer
struct UsingSpacerView: View {
    var body: some View {
        // puts the text to the 1/3 of the screen
        VStack {
            Spacer()
            Text("Hello World")
            Spacer()
            Spacer()
        }
        HStack {
            Text("Hello")
            Spacer()
            Text("World")
        }
    }
}

struct UsingSpacerView_Previews: PreviewProvider {
    static var previews: some View {
        UsingSpacerView()
    }
}
