//
//  VStackAndHStackView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-stacks-using-vstack-and-hstack
struct VStackAndHStackView: View {
    var body: some View {
        VStack {
            VStack {
                Text("SwiftUI")
                Text("rocks")
            }
            HStack {
                Text("SwiftUI")
                Text("rocks")
            }
        }
    }
}

struct VStackAndHStackView_Previews: PreviewProvider {
    static var previews: some View {
        VStackAndHStackView()
    }
}
