//
//  ScrollViewScrollToLocationView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-a-scroll-view-move-to-a-location-using-scrollviewreader
//New in iOS 14
struct ScrollViewScrollToLocationView: View {

    let colors: [Color] = [.red, .green, .blue]

        var body: some View {
            ScrollView {
                ScrollViewReader { value in
                    Button("Jump to #8") {
                        withAnimation(.easeIn) {
                            value.scrollTo(50, anchor: .center)
                                            }
                    }
                    .padding()

                    ForEach(0..<100) { i in
                        Text("Example \(i)")
                            .font(.title)
                            .frame(width: 200, height: 200)
                            .background(colors[i % colors.count])
                            .id(i)
                    }
                }
            }
//            .frame(height: 350)
        }
}

struct ScrollViewScrollToLocationView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewScrollToLocationView()
    }
}
