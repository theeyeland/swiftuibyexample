//
//  DiffrentLayoutsWithSizeClassesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-different-layouts-using-size-classes
struct DiffrentLayoutsWithSizeClassesView: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

        var body: some View {
            if horizontalSizeClass == .compact {
                Text("Compact")
            } else {
                Text("Regular")
            }
        }
}

struct DiffrentLayoutsWithSizeClassesView_Previews: PreviewProvider {
    static var previews: some View {
        DiffrentLayoutsWithSizeClassesView()
        // if you want to preview with diffrent size class - uncoment this line
//            .environment(\.horizontalSizeClass, .compact)
    }
}
