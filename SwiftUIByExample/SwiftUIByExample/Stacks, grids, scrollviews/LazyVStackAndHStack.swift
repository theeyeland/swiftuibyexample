//
//  LazyVStackAndHStack.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-lazy-load-views-using-lazyvstack-and-lazyhstack

//New in iOS 14
struct SampleRow: View {
    let id: Int

    var body: some View {
        Text("Row \(id)")
    }

    init(id: Int) {
        print("Loading row \(id)")
        self.id = id
    }
}

struct LazyVStackAndHStack: View {
    var body: some View {
        VStack {
            Divider()
            ScrollView {

                // LazyVStack takes the whole with of the screen , you can scroll in whole width with drugging , VStack - you can drug only on text
                // WARNING: once the Lazy text is loaded it remains in memory
                //                VStack {
                LazyVStack {
                    ForEach(1...100, id: \.self) { value in
                        Text("Row \(value)")
                    }
                }
            }
            .frame(height: 300)

            Divider()
            ScrollView {
                LazyVStack {
                    ForEach(1...100, id: \.self, content: SampleRow.init)
                }
            }
            .frame(height: 300)
            Divider()
        }
    }
}

struct LazyVStackAndHStack_Previews: PreviewProvider {
    static var previews: some View {
        LazyVStackAndHStack()
    }
}
