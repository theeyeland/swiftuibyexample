//
//  LayerViewsUsingZStackView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-layer-views-on-top-of-each-other-using-zstack
struct LayerViewsUsingZStackView: View {
    var body: some View {
        VStack {
            ZStackTest()
            Divider()
                .background(Color.red)
            ZStackTest(alignment: .leading)
            Spacer()
        }
    }
}

struct ZStackTest: View {

    var alignment: Alignment = .center

    var body: some View {
        ZStack(alignment: alignment) {
            Image("fresh-baked-croissant")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(10)
                .padding([.leading, .trailing],8)
            Text("Hacking with Swift")
                .font(.largeTitle)
                .background(Color.black)
                .cornerRadius(10)
                .foregroundColor(.white)
                .padding([.leading, .trailing],8)
        }
    }
}

struct LayerViewsUsingZStackView_Previews: PreviewProvider {
    static var previews: some View {
        LayerViewsUsingZStackView()
    }
}
