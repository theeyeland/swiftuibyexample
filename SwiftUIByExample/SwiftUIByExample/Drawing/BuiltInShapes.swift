//
//  BuiltInShapes.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

//Rectangle draws a box at the exact dimensions you specify.
//RoundedRectangle does the same, except now you can round the corners by a certain amount. Its second parameter, style, determines whether you want classic rounded corners (.circular) or Apple’s slightly smoother alternative (.continuous).
//Capsule draws a box where one edge axis is rounded fully, depending on whether the height or width is largest. Our shape is 100x50, so it will have rounded left and right edges while being straight on the top and bottom edges.
//Ellipse draws an ellipse at the exact dimensions you specify.
//Circle draws an ellipse where the height and width are equal, so when we provide 100x50 for the space we’ll actually get 50x50.

//https://www.hackingwithswift.com/quick-start/swiftui/swiftuis-built-in-shapes

struct BuiltInShapes: View {
    var body: some View {
        ZStack {
            Rectangle()
                .fill(Color.black)
                .frame(width: 200, height: 200)

            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.red)
                .frame(width: 200, height: 200)

            Capsule()
                .fill(Color.green)
                .frame(width: 100, height: 50)

            Ellipse()
                .fill(Color.blue)
                .frame(width: 100, height: 50)

            Circle()
                .fill(Color.white)
                .frame(width: 100, height: 50)
        }
    }
}

struct BuiltInShapes_Previews: PreviewProvider {
    static var previews: some View {
        BuiltInShapes()
    }
}
