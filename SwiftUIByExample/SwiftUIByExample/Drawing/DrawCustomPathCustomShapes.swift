//
//  DrawCustomPathCustomShapes.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

//you can use any paths you’ve previously built using CGPath or UIBezierPath, then convert the result to a SwiftUI path.
//
//If you want to use SwiftUI’s native Path type, create a variable instance of it then add as many points or shapes as you need. Don’t think about colors, fills, or stroke widths – you’re focusing on the raw shape here, and those kinds of settings are provided when your custom path is used.
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-draw-a-custom-path

struct ShrinkingSquares: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()

        for i in stride(from: 1, through: 100, by: 5.0) {
            let rect = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
            let insetRect = rect.insetBy(dx: CGFloat(i), dy: CGFloat(i))
            path.addRect(insetRect)
        }

        return path
    }
}

struct DrawCustomPathCustomShapes: View {
    var body: some View {
        ShrinkingSquares()
            .stroke()
            .foregroundColor(.yellow)
            .frame(width: 200, height: 200)
    }
}

struct DrawCustomPathCustomShapes_Previews: PreviewProvider {
    static var previews: some View {
        DrawCustomPathCustomShapes()
    }
}
