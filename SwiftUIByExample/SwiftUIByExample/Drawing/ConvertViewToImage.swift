//
//  ConvertViewToImage.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 27/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-convert-a-swiftui-view-to-an-image

// swiftUI don't have this possibility but we can use UIHostingController
extension View {
    func snapshot() -> UIImage {
        let controller = UIHostingController(rootView: self)
        let view = controller.view

        let targetSize = controller.view.intrinsicContentSize
        view?.bounds = CGRect(origin: .zero, size: targetSize)
        view?.backgroundColor = .clear

        let renderer = UIGraphicsImageRenderer(size: targetSize)

        return renderer.image { _ in
            view?.drawHierarchy(in: controller.view.bounds, afterScreenUpdates: true)
        }
    }
}

struct ConvertViewToImage: View {

//    To use that extension in SwiftUI, you should create your view as a property so you can reference it on demand – for example, in response to a button action.
    var textView: some View {
          Text("Hello, SwiftUI")
              .padding()
              .background(Color.blue)
              .foregroundColor(.white)
              .clipShape(Capsule())
      }

      var body: some View {
          VStack {
              textView

              Button("Save to image") {
                  let image = textView.snapshot()

                  UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
              }
          }
      }
}

struct ConvertViewToImage_Previews: PreviewProvider {
    static var previews: some View {
        ConvertViewToImage()
    }
}
