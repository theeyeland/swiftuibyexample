//
//  HowToDetectDarkMode.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 31/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-dark-mode
struct HowToDetectDarkMode: View {
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        Text(colorScheme == .dark ? "In dark mode" : "In light mode")
    }
}

struct HowToDetectDarkMode_Previews: PreviewProvider {
    static var previews: some View {
        HowToDetectDarkMode()
    }
}
