//
//  HowToReduceAnimation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 31/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-reduce-animations-when-requested

// withAnimation() do not respect reduceMotion accessibillty
// but we can help with this global function - withAnimation() is also global function
func withOptionalAnimation<Result>(_ animation: Animation? = .default, _ body: () throws -> Result) rethrows -> Result {
    if UIAccessibility.isReduceMotionEnabled {
        return try body()
    } else {
        return try withAnimation(animation, body)
    }
}

struct HowToReduceAnimation: View {
    @State private var scale: CGFloat = 1

    var body: some View {
        Text("Hello, World!")
            .scaleEffect(scale)
            .onTapGesture {
                withOptionalAnimation {
                    scale *= 1.5
                }
            }
    }
}

struct HowToReduceAnimation_Previews: PreviewProvider {
    static var previews: some View {
        HowToReduceAnimation()
    }
}
