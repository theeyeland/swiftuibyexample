//
//  DecorativeImagesToPreventScreenReaderClutter.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 31/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-decorative-images-to-reduce-screen-reader-clutter
struct DecorativeImagesToPreventScreenReaderClutter: View {
    var body: some View {
        Image(decorative: "full-english")
    }
}

struct DecorativeImagesToPreventScreenReaderClutter_Previews: PreviewProvider {
    static var previews: some View {
        DecorativeImagesToPreventScreenReaderClutter()
    }
}
