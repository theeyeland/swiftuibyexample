//
//  ReduceMotionAccessibility.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 31/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-the-reduce-motion-accessibility-setting
struct ReduceMotionAccessibility: View {
    @Environment(\.accessibilityReduceMotion) var reduceMotion
    @State private var scale: CGFloat = 1

    var body: some View {
        VStack {
            Spacer()

            Circle()
                .frame(width: 20, height: 20)
                .scaleEffect(scale)
                .animation(reduceMotion ? nil : .spring(response: 1, dampingFraction: 0.1))

            Spacer()

            Button("Increase Scale") {
                scale *= 1.5
            }
        }
    }
}

struct ReduceMotionAccessibility_Previews: PreviewProvider {
    static var previews: some View {
        ReduceMotionAccessibility()
    }
}
