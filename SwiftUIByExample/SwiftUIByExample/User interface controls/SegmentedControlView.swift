//
//  SegmentedControlView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

//https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-segmented-control-and-read-values-from-it
struct SegmentedControlView: View {
    @State private var favoriteColor = 0
    @State private var favoriteColorVer2 = "Red"
        var colors = ["Red", "Green", "Blue"]

    var body: some View {
        VStack {
            Picker(selection: $favoriteColor, label: Text("What is your favorite color?")) {
                Text("Red").tag(0)
                Text("Green").tag(1)
                Text("Blue").tag(2)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(8)

            Text("Value: \(favoriteColor)")

            Divider().padding(8)

            Picker(selection: $favoriteColorVer2, label: Text("What is your favorite color?")) {
                ForEach(colors, id: \.self) {
                    Text($0)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(8)

            Text("Value: \(favoriteColorVer2)")
        }
    }
}

struct SegmentedControlView_Previews: PreviewProvider {
    static var previews: some View {
        SegmentedControlView()
    }
}
