//
//  ToggleSwitchView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-toggle-switch
struct ToggleSwitchView: View {
    @State private var showGreeting = true

        var body: some View {
            VStack {
                Toggle("Show welcome message", isOn: $showGreeting)
                    .toggleStyle(SwitchToggleStyle(tint: .red)) // this is the only way how to set color on toggle

                if showGreeting {
                    Text("Hello World!")
                } else {
                    Text("Hello World!").hidden()
                }
            }
        }
}

struct ToggleSwitchView_Previews: PreviewProvider {
    static var previews: some View {
        ToggleSwitchView()
    }
}
