//
//  SecureTextField.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-secure-text-fields-using-securefield
struct SecureTextFieldView: View {
    @State private var password: String = ""

        var body: some View {
            VStack {
                SecureField("Enter a password", text: $password)
                Text("You entered: \(password)")
            }
        }
}

struct SecureTextField_Previews: PreviewProvider {
    static var previews: some View {
        SecureTextFieldView()
    }
}
