//
//  StepperView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-stepper-and-read-values-from-it
struct StepperView: View {
    @State private var age = 18

       var body: some View {
           VStack {
               Stepper("Enter your age", value: $age, in: 0...130)
                .padding()
               Text("Your age is \(age)")

            Divider().padding()
            Stepper("Enter your age", onIncrement: {
                age += 1
            }, onDecrement: {
                age -= 1
            }).padding()

            Text("Your age is \(age)")
           }
       }
}

struct StepperView_Previews: PreviewProvider {
    static var previews: some View {
        StepperView()
    }
}
