//
//  UsingProgressView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI
import Combine

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-progress-on-a-task-using-progressview
// New in iOS 14
extension Publisher where Failure == Never {
    func assign<Root: AnyObject>(
        to keyPath: ReferenceWritableKeyPath<Root, Output>,
        onWeak object: Root
    ) -> AnyCancellable {
        sink { [weak object] value in
            object?[keyPath: keyPath] = value
        }
    }
}

extension Publisher where Failure == Never {
    func assign<Root: AnyObject>(
        to keyPath: ReferenceWritableKeyPath<Root, Output>,
        onUnowned object: Root
    ) -> AnyCancellable {
        sink { [unowned object] value in
            object[keyPath: keyPath] = value
        }
    }
}

class MyTimer: ObservableObject {

    @Published var timerValue: Date = Date()

    var currentTimePublisher: Timer.TimerPublisher? = Timer.TimerPublisher(interval: 0.1, runLoop: .main, mode: .default)
    var cancellableSet = Set<AnyCancellable>()

    func start() {
        if !cancellableSet.isEmpty {
            stop()
        }

        currentTimePublisher = Timer.TimerPublisher(interval: 0.1, runLoop: .main, mode: .default)

        if let currentTimePublisher = currentTimePublisher {

//            currentTimePublisher.sink { [unowned self] (date) in
//                self.timerValue = date
//            }.store(in:&self.cancellableSet)

            currentTimePublisher.assign(to: \.timerValue, onUnowned: self).store(in: &cancellableSet)

            currentTimePublisher.connect().store(in:&self.cancellableSet)
        }
    }

    func stop() {

        cancellableSet.forEach {
            $0.cancel()
        }

        cancellableSet.removeAll()
        currentTimePublisher = nil
    }

    deinit {
        stop()
    }
}

struct UsingProgressView: View {
    @State private var downloadAmount = 0.0
    @ObservedObject var timer = MyTimer()// Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()

    var body: some View {
        VStack {
            Button("Start") {
                downloadAmount = 0.0
                timer.start()
            }
            ProgressView("Downloading…", value: downloadAmount, total: 100)
                .padding()
        }
        .onReceive(timer.$timerValue.dropFirst()) { value in
            print("timer value: \(value)")
            if downloadAmount < 100 {
                downloadAmount += 2
            } else {
                timer.stop()
//                timer.upstream.connect().cancel()
            }
        }
    }
}

struct UsingProgressView_Previews: PreviewProvider {
    static var previews: some View {
        UsingProgressView()
    }
}
