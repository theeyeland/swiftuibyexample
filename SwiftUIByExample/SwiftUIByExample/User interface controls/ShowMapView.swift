//
//  ShowMapView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI
import MapKit

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-a-map-view
// https://www.hackingwithswift.com/forums/swiftui/how-can-i-show-a-users-location-on-a-map-on-first-draw-then-ignore-location-updates-after-that/3928
// New in iOS 14
struct ShowMapView: View {
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))

    var body: some View {
        VStack {
            Map(coordinateRegion: $region)
                .frame(width: 400, height: 200)

            // Map vidout interaction possibilities or just zoom possibility [.zoom]
            Map(coordinateRegion: .constant(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))), interactionModes: [.zoom])
                .frame(width: 400, height: 200)

            // show user location and follow
            Map(coordinateRegion: $region, showsUserLocation: true, userTrackingMode: .constant(.follow))
                .frame(width: 400, height: 200)
        }
    }
}

struct ShowMapView_Previews: PreviewProvider {
    static var previews: some View {
        ShowMapView()
    }
}
