//
//  ReadingTextFromTextField.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-read-text-from-a-textfield
struct ReadingTextFromTextField: View {
    @State private var name: String = "Tim"

    var body: some View {
        VStack(alignment: .leading) {
            TextField("Placeholder" , text: $name)
            Text("Hello, \(name)!")
        }
    }
}

struct ReadingTextFromTextField_Previews: PreviewProvider {
    static var previews: some View {
        ReadingTextFromTextField()
    }
}
