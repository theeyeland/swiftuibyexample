//
//  ModifyTextFieldView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

//https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-a-border-to-a-textfield
//https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-a-placeholder-to-a-textfield
//https://www.hackingwithswift.com/quick-start/swiftui/how-to-disable-autocorrect-in-a-textfield
struct ModifyTextFieldView: View {
    @State private var name: String = ""

    var body: some View {
        VStack(alignment: .leading) {
            TextField("Enter your name", text: $name) // "Enter your Name is placeholder"
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .disableAutocorrection(true) // autocorrect is default ON
                .padding()
            Text("Hello, \(name)!")
                .padding()
        }
    }
}

struct ModifyTextFieldView_Previews: PreviewProvider {
    static var previews: some View {
        ModifyTextFieldView()
    }
}
