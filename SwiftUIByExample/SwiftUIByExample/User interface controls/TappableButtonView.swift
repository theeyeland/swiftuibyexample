//
//  TappableButtonView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-a-tappable-button
struct TappableButtonView: View {
    @State private var showDetails = false

        var body: some View {
            VStack(alignment: .center) {
                Button {
                    showDetails.toggle()
                    print("Image tapped!")
                } label: {
//                    Text("Show details")
//                        .font(.title)
                    // label can be any view e.g. Image
                    Image(systemName: "cloud.heavyrain.fill")
                        .font(.largeTitle)
                        .foregroundColor(Color.red)
                        .frame(maxWidth: .infinity)
                }
                .frame(maxWidth: .infinity)
                .background(Color.yellow)

                if showDetails {
                    Text("You should follow me on Twitter: @twostraws")
                        .font(.largeTitle)
                }

                Button {
                    print("Button pressed")
                } label: {
                    Text("Press Me")
                        .padding(20)
                }
                .contentShape(Rectangle())
            }
        }
}

struct TappableButtonView_Previews: PreviewProvider {
    static var previews: some View {
        TappableButtonView()
    }
}
