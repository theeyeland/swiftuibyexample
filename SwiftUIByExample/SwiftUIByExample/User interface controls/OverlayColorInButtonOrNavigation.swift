//
//  OverlayColorInButtonOrNavigation.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-disable-the-overlay-color-for-images-inside-button-and-navigationlink
struct OverlayColorInButtonOrNavigation: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: Text("Detail view here")) {
                    Image("full-english")
                }
                NavigationLink(destination: Text("Detail view here")) {
                    Image("full-english")
                        .renderingMode(.original)
                }
                NavigationLink(destination: Text("Detail view here")) {
                    Image("full-english")
                }
                .buttonStyle(PlainButtonStyle())
            }
            .navigationTitle("Tap Images for diffrence")
        }
    }
}

struct OverlayColorInButtonOrNavigation_Previews: PreviewProvider {
    static var previews: some View {
        OverlayColorInButtonOrNavigation()
    }
}
