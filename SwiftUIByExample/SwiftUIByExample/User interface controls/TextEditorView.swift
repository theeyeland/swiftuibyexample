//
//  TextEditorView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-multi-line-editable-text-with-texteditor
// New in iOS 14
struct TextEditorView: View {
    @State private var profileText: String = "Enter your bio"

        var body: some View {
            NavigationView {
                TextEditor(text: $profileText)
                    .foregroundColor(.secondary)
                    .font(.custom("HelveticaNeue", size: 20))
            }
        }
}

struct TextEditorView_Previews: PreviewProvider {
    static var previews: some View {
        TextEditorView()
    }
}
