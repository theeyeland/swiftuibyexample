//
//  SpinnerIndeterminateProgressView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-indeterminate-progress-using-progressview
// New in iOS 14
struct SpinnerIndeterminateProgressView: View {
    var body: some View {
        ProgressView("Downloading…")
    }
}

struct SpinnerIndeterminateProgressView_Previews: PreviewProvider {
    static var previews: some View {
        SpinnerIndeterminateProgressView()
    }
}
