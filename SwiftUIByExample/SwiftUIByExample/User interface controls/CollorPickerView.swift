//
//  CollorPickerView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-let-users-select-a-color-with-colorpicker
// New in iOS 14
struct CollorPickerView: View {
    @State private var bgColor = Color.red

        var body: some View {
            VStack {
                ColorPicker("Set the background color opacity ON", selection: $bgColor, supportsOpacity: true )
                ColorPicker("Set the background color - Opacity OFF", selection: $bgColor, supportsOpacity: false )
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(bgColor)
        }
}

struct CollorPickerView_Previews: PreviewProvider {
    static var previews: some View {
        CollorPickerView()
    }
}
