//
//  OpenWebLinksInSafariView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-open-web-links-in-safari
// New in iOS 14
struct OpenWebLinksInSafariView: View {

    @Environment(\.openURL) var openURL

    var body: some View {
        VStack {
            Link("Learn SwiftUI", destination: URL(string: "https://www.hackingwithswift.com/quick-start/swiftui")!)
            Divider()
            Link("Visit Apple", destination: URL(string: "https://www.apple.com")!)
                .font(.title)
                .foregroundColor(.red)
            Divider()
            Link(destination: URL(string: "https://www.apple.com")!) {
                Image(systemName: "link.circle.fill")
                    .font(.largeTitle)
            }
            Divider()
            Button("Visit Apple") {
                openURL(URL(string: "https://www.apple.com")!)
            }
        }
    }
}

struct OpenWebLinksInSafariView_Previews: PreviewProvider {
    static var previews: some View {
        OpenWebLinksInSafariView()
    }
}
