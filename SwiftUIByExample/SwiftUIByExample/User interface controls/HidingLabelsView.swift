//
//  HidingLabelsView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-hide-the-label-of-a-picker-stepper-toggle-and-more-using-labelshidden
struct HidingLabelsView: View {
    @State private var showGreeting = false
    @State private var selectedNumber = 0
    @State private var date = Date()

    var body: some View {
        VStack {
            Toggle("Show welcome message", isOn: $showGreeting)
            Picker("Select a number", selection: $selectedNumber) {
                ForEach(0..<10) {
                    Text("\($0)")
                }
            }
            DatePicker("Enter your birthday", selection: $date)
                .datePickerStyle(WheelDatePickerStyle())
        }.labelsHidden()

        // Note labels are used with voice over so do not use EmptyViews for labels
        // e.g.
//        Picker(selection: $selectedNumber, label: EmptyView()) {
//                    ForEach(0..<10) {
//                        Text("\($0)")
//                    }
//                }
    }
}

struct HidingLabelsView_Previews: PreviewProvider {
    static var previews: some View {
        HidingLabelsView()
    }
}
