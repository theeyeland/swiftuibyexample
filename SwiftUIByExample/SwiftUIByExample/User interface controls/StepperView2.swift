//
//  StepperView2.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 20/05/2021.
//

import SwiftUI
import Combine

struct StepperView2: View {

    @ObservedObject var viewModel: StepperViewModel = StepperViewModel()

    var body: some View {
        VStack {
            Stepper(onIncrement: {
                viewModel.incrementStep()
            }, onDecrement: {
                viewModel.decrementStep()
            }) {
                Text("Value: \(viewModel.value) Color: \(viewModel.currentColor.description)")
            }
            .padding(8)
            .background(viewModel.currentColor)
            Divider()
            .padding(8)
            Stepper("Select Color", value: $viewModel.value, in: 0...6)
            .padding(8)
            Text("selectee color: \(viewModel.currentColor.description)")
                .background(viewModel.currentColor)
                Divider()
        }
    }
}

struct StepperView2_Previews: PreviewProvider {
    static var previews: some View {
        StepperView2()
    }
}

class BaseViewModel{

    lazy var cancellableSet: Set<AnyCancellable> = []

    deinit {
        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func initialize() {}
}

class StepperViewModel: BaseViewModel, ObservableObject {

    @Published var value: Int = 0
    let colors: [Color] = [.orange, .red, .gray, .blue,
                           .green, .purple, .pink]

    var currentColor: Color {
       colors[value]
    }

    override init() {
        super.init()
        self.initialize()
    }

    override func initialize() {
        $value.sink { (value) in
            print(value)
        }.store(in: &cancellableSet)
    }

    func incrementStep() {
        print("before increment \(value)")
        self.value += 1
        print("after increment \(value)")
        if value >= colors.count { self.value = 0 }
    }

    func decrementStep() {
        print("before decrement \(value)")
        self.value -= 1
        print("after decrement \(value)")
        if value < 0 { self.value = colors.count - 1 }
    }
}
