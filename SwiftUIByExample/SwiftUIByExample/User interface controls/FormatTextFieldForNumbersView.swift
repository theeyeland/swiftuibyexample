//
//  FormatTextFieldForNumbersView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 19/05/2021.
//

import SwiftUI

// it is not working very good - when you write floating number it crash
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-format-a-textfield-for-numbers
struct FormatTextFieldForNumbersView: View {
    @State private var score = 0

        let formatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            return formatter
        }()

        var body: some View {
            VStack {
                TextField("Amount to transfer", value: $score, formatter: formatter)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()

                Text("Your score was \(score).")
            }
        }
}

struct FormatTextFieldForNumbersView_Previews: PreviewProvider {
    static var previews: some View {
        FormatTextFieldForNumbersView()
    }
}
