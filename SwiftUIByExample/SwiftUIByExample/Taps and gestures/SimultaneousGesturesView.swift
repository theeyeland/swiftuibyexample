//
//  SimultaneousGesturesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-make-two-gestures-recognize-at-the-same-time-using-simultaneousgesture
struct SimultaneousGesturesView: View {
    var body: some View {
            VStack {
                Circle()
                    .fill(Color.red)
                    .frame(width: 200, height: 200)
                    .onTapGesture {
                        print("Circle tapped")
                    }
            }
            .simultaneousGesture( // both gesture are done - without this only child gesture will be executed - chald has priority !!! this must be added to parent
                TapGesture()
                    .onEnded { _ in
                        print("VStack tapped")
                    }
            )
        }
}

struct SimultaneousGesturesView_Previews: PreviewProvider {
    static var previews: some View {
        SimultaneousGesturesView()
    }
}
