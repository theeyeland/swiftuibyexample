//
//  PrioritizedGestureView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-force-one-gesture-to-recognize-before-another-using-highprioritygesture
struct PrioritizedGestureView: View {
    var body: some View {
        VStack {
            Circle()
                .fill(Color.red)
                .frame(width: 200, height: 200)
                .onTapGesture { // child has a priority on gasture unless highPriorityGesture is used in Parent
                    print("Circle tapped")
                }
        }
        .padding()
        .frame(minWidth:0, maxWidth: .infinity)
        .background(Color.blue)
//        .onTapGesture {
//            print("VStack tapped")
//        }
        .highPriorityGesture(
            TapGesture()
                .onEnded { _ in
                    print("VStack tapped")
                }
        )
    }
}

struct PrioritizedGestureView_Previews: PreviewProvider {
    static var previews: some View {
        PrioritizedGestureView()
    }
}
