//
//  SequencedGesturesView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-create-gesture-chains-using-sequencedbefore
struct SequencedGesturesView: View {
    @State private var message = "Long press then drag"

       var body: some View {
           let longPress = LongPressGesture()
               .onEnded { _ in
                   message = "Now drag me"
               }

           let drag = DragGesture()
               .onEnded { _ in
                   message = "Success!"
               }

           let combined = longPress.sequenced(before: drag)

           Text(message)
               .gesture(combined)
       }
}

struct SequencedGesturesView_Previews: PreviewProvider {
    static var previews: some View {
        SequencedGesturesView()
    }
}
