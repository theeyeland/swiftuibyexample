//
//  GestureRecognizersView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-add-a-gesture-recognizer-to-a-view
struct GestureRecognizersView: View {
    @State private var scale: CGFloat = 1.0
    @State private var dragCompleted = false
    @State private var dragOffset = CGSize.zero

    var body: some View {
        VStack {
            Image("fresh-baked-croissant")
                .scaleEffect(scale)
                .offset(dragOffset)
                .gesture(
                    TapGesture()
                        .onEnded { _ in
                            scale -= 0.1
                        }
                )
                .gesture(
                    LongPressGesture(minimumDuration: 1)
                        .onEnded { _ in
                            scale /= 2
                        }
                )
                .gesture(
                    DragGesture()
                        .onChanged { gesture in
                            dragOffset = gesture.translation
                        }
                        .onEnded { gesture in
                            dragOffset = .zero
                            dragCompleted = true
                        }
                )
            if dragCompleted {
                Text("Drag completed!")
            }
        }
    }
}

struct GestureRecognizersView_Previews: PreviewProvider {
    static var previews: some View {
        GestureRecognizersView()
    }
}
