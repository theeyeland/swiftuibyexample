//
//  AllowHitTestingView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-disable-taps-for-a-view-using-allowshittesting
struct AllowHitTestingView: View {
    var body: some View {
        ZStack {
            Button("Tap Me") {
                print("Button was tapped")
            }
            .frame(width: 100, height: 100)
            .background(Color.white)

            Rectangle()
                .fill(Color.red.opacity(0.2))
                .frame(width: 300, height: 300)
                .clipShape(Circle())
                .allowsHitTesting(false) // rectagle is over button and caches the tap, with allow hit testing to false, the tap pass trought the rectange to the button
        }
    }
}

struct AllowHitTestingView_Previews: PreviewProvider {
    static var previews: some View {
        AllowHitTestingView()
    }
}
