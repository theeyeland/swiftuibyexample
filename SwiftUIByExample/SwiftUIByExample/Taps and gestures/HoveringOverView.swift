//
//  HoveringOverView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-the-user-hovering-over-a-view
struct HoveringOverView: View {
    @State private var overText = false

    let hoverEffects: [HoverEffect] = [.automatic, .highlight, .lift]
    @State private var selectedHoverEfect = 0

    var body: some View {
        VStack {
            Text("Hello, World!")
                .foregroundColor(overText ? .green : .red)
                .onHover { over in
                    overText = over
                }

            Divider()
            Text("Tap me!")
                .font(.largeTitle)
                .hoverEffect(hoverEffects[selectedHoverEfect])
                .onTapGesture {
                    print("Text tapped")
                }
            Picker(selection: $selectedHoverEfect, label: Text("Select Hover Effect")) {
                Text("Automatic").tag(0)
                Text("highlight").tag(1)
                Text("lift").tag(2)
            }
        }
    }
}

struct HoveringOverView_Previews: PreviewProvider {
    static var previews: some View {
        HoveringOverView()
    }
}
