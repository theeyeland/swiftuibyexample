//
//  DoubleTapActionView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-read-tap-and-double-tap-gestures
struct DoubleTapActionView: View {
    var body: some View {
        VStack {
            Text("Tap me").onTapGesture {
                print("tap")
            }
            Divider()
            Image("full-english")
                .onTapGesture(count: 2) {
                    print("Double tapped!")
                }
        }
    }
}

struct DoubleTapActionView_Previews: PreviewProvider {
    static var previews: some View {
        DoubleTapActionView()
    }
}
