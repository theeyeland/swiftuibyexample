//
//  ControlTapableAreaView.swift
//  SwiftUIByExample
//
//  Created by Zoltan Bognar The EyeLand on 21/05/2021.
//

import SwiftUI

// https://www.hackingwithswift.com/quick-start/swiftui/how-to-control-the-tappable-area-of-a-view-using-contentshape
struct ControlTapableAreaView: View {
    var body: some View {
        VStack {
            Image(systemName: "person.circle").resizable().frame(width: 50, height: 50)
            Spacer().frame(height: 50)
            Text("Paul Hudson")
        }
//        .padding()
//        .frame(minWidth:0, maxWidth: .infinity)
//        .background(Color.blue)
        .contentShape(Rectangle()) // if you use this than tap gesture works not only on image or text, but adding background color do the same thing hereit here too
        .onTapGesture {
            print("Show details for user")
        }
    }
}

struct ControlTapableAreaView_Previews: PreviewProvider {
    static var previews: some View {
        ControlTapableAreaView()
    }
}
