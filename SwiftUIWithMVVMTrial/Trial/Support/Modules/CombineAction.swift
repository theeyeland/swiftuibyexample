//
//  CombineAction.swift
//  WheeledMap
//
//  Created by Zoltan Bognar TheEyeLand on 29/10/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Combine

struct Combine {

    class Action<Input, Out, Error: Swift.Error>: BaseDebugDeallocProtocol {

        deinit {
            log.theeyeland.debugDealloc(withObject: self)
        }

        private var cancelable: Cancellable?

        var values: PassthroughSubject<Out, Never> = PassthroughSubject<Out, Never>()
        var errors: PassthroughSubject<Error, Never> = PassthroughSubject<Error, Never>()
        @Published private var _completed: Void = ()

        var completed: AnyPublisher<Void, Never> {
            // we have to drop the first value , becouse Published variable sends the default value to the subscriber when it subscribes
            return $_completed.dropFirst().eraseToAnyPublisher()
        }

        @Published var isExecuting: Bool = false

        private var execute: (Action<Input, Out, Error>, Input) -> AnyPublisher<Out, Error>

        init (execute: @escaping (Input) -> AnyPublisher<Out, Error>) {

            self.execute = { action, input in
                execute(input)
            }
        }

        func apply(value: Input) {

            if let cancelable = cancelable {
                cancelable.cancel()
            }
            cancelable = nil

            isExecuting = true
            cancelable = execute(self, value).sink { [unowned self] (completion) in
                switch completion {
                case .finished:
                    self.isExecuting = false
                    self._completed = ()
                    log.debug("finished")
                case .failure(let anError):
                    self.isExecuting = false
                    self._completed = ()
                    self.errors.send(anError)
                    log.debug("received error: \(anError)")
                }
            } receiveValue: { [unowned self] (value) in
                self.values.send(value)
            }
        }
    }
}

extension Combine.Action where Input == Void {

    func apply() {
        self.apply(value: ())
    }
}
