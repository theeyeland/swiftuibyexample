//
//  MultiValuePublisher.swift
//  WheeledMap
//
//  Created by Zoltan Bognar TheEyeLand on 04/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Combine

public struct MultiValuePublisher<Value, Error: Swift.Error>: Publisher {
    public init(_ run: @escaping (PassthroughSubject<Value, Error>) -> Void ) {
        self.run = run
    }

    public var run: (PassthroughSubject<Value, Error>) -> Void

    public typealias Output = Value
    public typealias Failure = Error

    public func receive<Downstream: Subscriber>(subscriber: Downstream) where Downstream.Input == Output, Downstream.Failure == Failure {
        let progressSubject = PassthroughSubject<Value, Error>()
        progressSubject.subscribe(subscriber)
        run(progressSubject)
    }
}
