//
//  RootCoordinatorAssebly.swift
//  SlovakLines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 16.07.18.
//  Copyright © 2018 freevision s.r.o. All rights reserved.
//

import Foundation
import Swinject

class RootCoordinatorAssebly: BaseAssembly {

    public override func assemble(container: Container) {
        container.register(RootCoordinatorProtocol.self) { resolver in
            let coordinator = RootCoordinator()
            return coordinator
        }
    }
}
