//
//  DateExtension.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation

extension Date {

    static var secondsPerMinute: TimeInterval {
        return 60
    }
    static var secondsPerHour: TimeInterval {
        return 3600
    }

    static var secondsPerDay: TimeInterval {
        return 24 * Date.secondsPerHour
    }

    static var today: Date {
       return Calendar.current.startOfDay(for: Date())
    }

    func isToday() -> Bool {
       return Calendar.current.isDateInToday(self)
    }

    func isSameDay(asDate date: Date) -> Bool {
       return Calendar.current.isDate(self, inSameDayAs: date)
    }

    static var tomorrow: Date {
        return Calendar.current.startOfDay(for: Date().addingTimeInterval(Date.secondsPerDay))
    }

    func startOfDay(byAddingDays days: Int) -> Date {
        return Calendar.current.startOfDay(for: self.addingTimeInterval(TimeInterval(days) * Date.secondsPerDay))
    }

    var formattedAsRelativeDayNameWithoutYear: String {
        let formater = DateFormatter()

        if Calendar.current.startOfDay(for: self) >= Calendar.current.startOfDay(for: Date.today) &&
            Calendar.current.startOfDay(for: self) <= Date().startOfDay(byAddingDays: 2) {
            formater.dateStyle = .short
            formater.doesRelativeDateFormatting = true
        } else {
            formater.dateFormat = "d.M."
        }

        return formater.string(from: self)
    }

    var formattedAsRelativeDayName: String {
        let formater = DateFormatter()
        formater.dateStyle = .short
        formater.timeStyle = .short
        formater.doesRelativeDateFormatting = true
        if !isSameDay(asDate: Date()) {
            formater.doesRelativeDateFormatting = false
        }
        return formater.string(from: self)
    }

    var formattedAsShortDayName: String {
        let formater = DateFormatter()
        formater.dateFormat = "EEE"
        return formater.string(from: self)
    }

    var formattedAsDate: String {
        let formater = DateFormatter()
        formater.dateFormat = "d.M."
        return formater.string(from: self)
    }

    var formattedAsTime: String {
        let formater = DateFormatter()
        formater.dateFormat = "H:mm"
        return formater.string(from: self)
    }

    var formattedForAnalytics: String {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd"
        return formater.string(from: self)
    }
}
