//
//  PubllisherExtension.swift
//  WheeledMap
//
//  Created by Zoltan Bognar TheEyeLand on 02/11/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Combine

public protocol OptionalType {

  associatedtype Wrapped
  var value: Wrapped? { get }
}

extension Optional: OptionalType {

  public var value: Wrapped? {
    return self
  }
}

public extension Publisher where Self.Output: OptionalType {
  func skipNil() -> AnyPublisher<Self.Output.Wrapped, Self.Failure> {
    return self.flatMap { element -> AnyPublisher<Self.Output.Wrapped, Self.Failure> in
      guard
        let value = element.value
        else { return Empty(completeImmediately: false).setFailureType(to: Self.Failure.self).eraseToAnyPublisher() }
      return Just(value).setFailureType(to: Self.Failure.self).eraseToAnyPublisher()
    }.eraseToAnyPublisher()
  }
}

extension Publisher where Failure == Never {
    func assign<Root: AnyObject>(
        to keyPath: ReferenceWritableKeyPath<Root, Output>,
        onWeak object: Root
    ) -> AnyCancellable {
        sink { [weak object] value in
            object?[keyPath: keyPath] = value
        }
    }
}

extension Publisher where Failure == Never {
    func assign<Root: AnyObject>(
        to keyPath: ReferenceWritableKeyPath<Root, Output>,
        onUnowned object: Root
    ) -> AnyCancellable {
        sink { [unowned object] value in
            object[keyPath: keyPath] = value
        }
    }
}
