//
//  DoubleExtension.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation

extension Double {

    var formattedToEUR: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = "EUR"

        return formatter.string(from: NSNumber(value: self)) ?? "--"
    }
}
