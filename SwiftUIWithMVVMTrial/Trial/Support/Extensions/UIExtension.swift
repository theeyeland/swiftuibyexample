//
//  UIFontExtension.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 11.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import UIKit

extension UIFont {

    public class func fontOrDefault(name fontName: String, size: CGFloat) -> UIFont {
        guard let newFont = UIFont(name: fontName, size: size) else { return UIFont.systemFont(ofSize: size) }
        return newFont
    }
}

extension UIUserInterfaceIdiom {

    public static func acquire() -> UIUserInterfaceIdiom {
        return UIDevice.current.userInterfaceIdiom
    }

    public static func isIPad() -> Bool {
        return acquire() == .pad
    }

    public static func isIPadPro() -> Bool {
        return isIPad() && (UIScreen.main.bounds.size.height == 1366 || UIScreen.main.bounds.size.width == 1366)
    }

    public static func isIPhone() -> Bool {
        return acquire() == .phone
    }

}

// UIDevice extension wrapper for objective-c language
extension UIDevice {

    public class func isIPad() -> Bool {
        return UIUserInterfaceIdiom.isIPad()
    }

    public class func isIPadPro() -> Bool {
        return UIUserInterfaceIdiom.isIPadPro()
    }

    public class func isIPhone() -> Bool {
        return UIUserInterfaceIdiom.isIPhone()
    }
}

extension UIWindow {

    static var isLandscape: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows
                .first?
                .windowScene?
                .interfaceOrientation
                .isLandscape ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }

    static var isPortrait: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows
                .first?
                .windowScene?
                .interfaceOrientation
                .isPortrait ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isPortrait
        }
    }
}
