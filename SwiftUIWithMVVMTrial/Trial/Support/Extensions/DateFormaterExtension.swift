//
//  DateFormaterExtension.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 12.04.18.
//  Copyright © 2021 TheEyeLand All rights reserved.
//

import Foundation

extension DateFormatter {

    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
       // formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let iso8601Date: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        // formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let classicDate: DateFormatter = {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZ"
        return formater
    }()
}
