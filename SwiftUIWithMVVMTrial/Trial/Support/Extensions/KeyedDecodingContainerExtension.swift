//
//  KeyedDecodingContainerExtension.swift
//  Slovak Lines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 16.10.18.
//  Copyright © 2021 TheEyeLand All rights reserved.
//

import Foundation

extension KeyedDecodingContainer {

    public func decode<T>(_ key: KeyedDecodingContainer<K>.Key, withDefault defaultValue: T, as type: T.Type = T.self, printError logError: Bool = false) -> T where T: Decodable {
        do {
            let value = try decode(type, forKey: key)
            return value
        } catch {
            if logError {
                log.debug(error, userInfo: [tags: MainApp.Constants.Logger.mapping])
                log.debug(error.localizedDescription + "\n using default value \(defaultValue)", userInfo: [tags: MainApp.Constants.Logger.mapping])
            }
        }

        return defaultValue
    }
}

private struct JSONCodingKeys: CodingKey {

    internal var stringValue: String

    internal var intValue: Int?

    internal init(value: String) {
        self.stringValue = value
    }

    internal init?(stringValue: String) {
        self.stringValue = stringValue
    }

    internal init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }

}

extension KeyedEncodingContainer {

    /// Encodes the given value for the given key.
    ///
    /// - parameter value: The value to encode.
    /// - parameter key: The key to associate the value with.
    /// - throws: `EncodingError.invalidValue` if the given value is invalid in
    ///   the current context for this format.
    mutating func encode(_ value: [String: Any], forKey key: KeyedEncodingContainer.Key) throws { // swiftlint:disable:this explicit_acl
        var container = nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)

        try value.forEach { (key: String, value: Any) in
            let codingKey = JSONCodingKeys(value: key)

            switch value {
            case let boolValue as Bool:
                try container.encode(boolValue, forKey: codingKey)
            case let stringValue as String:
                try container.encode(stringValue, forKey: codingKey)
            case let intValue as Int:
                try container.encode(intValue, forKey: codingKey)
            case let doubleValue as Double:
                try container.encode(doubleValue, forKey: codingKey)
            case let nestedDictionary as [String: Any]:
                try container.encode(nestedDictionary, forKey: codingKey)
            default:
                let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Failed to encode dictionary value.")

                throw EncodingError.invalidValue(value, context)
            }
        }
    }

}
