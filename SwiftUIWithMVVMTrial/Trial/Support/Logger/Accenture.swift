//
//  TheEyeLand.swift
//  TheEyeLand
//
//  Created by Zoltan Bognar on 08.12.16.
//  Copyright © 2021 TheEyeLand All rights reserved.
//

public protocol TheEyeLandExtensionsProvider: class {}

extension TheEyeLandExtensionsProvider {

    /// A proxy which hosts rsframework extensions for `self`.
    public var theeyeland: TheEyeLand<Self> {
        return TheEyeLand(self)
    }

    /// A proxy which hosts static rsframework extensions for the type of `self`.
    public static var theeyeland: TheEyeLand<Self>.Type {
        return TheEyeLand<Self>.self
    }
}

// A `RSFramework` proxy hosts rsframework extensions to `Base`.
public struct TheEyeLand<Base> {

    public let base: Base

    // Construct a proxy.
    //
    // - parameters:
    //   - base: The object to be proxied.
    fileprivate init(_ base: Base) {
        self.base = base
    }
}
