//
//  Constants.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2021 TheEyeLand All rights reserved.
//

import Foundation
import XCGLogger

public struct Constant {

    private static let description: String = "Constant"

    public struct Dealloc {

        private static let description: String = "Dealloc"
        public static let Common = "Common"
        public static let ViewModel = "ViewModel"
        public static let ViewController = "ViewController"
        public static let ViewControllerCell = "ViewControllerCell"
        public static let View = "View"
        public static let Interactor = "Interactor"
        public static let Coordinator = "Coordinator"
        public static let DataProvider = "DataProvider"
        public static let Assembly = "Assembly"
    }
}
