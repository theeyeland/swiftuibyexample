//
//  TransactionDetailViewModel.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

class TransactionDetailViewModel: BaseViewModel, ObservableObject {

    private let interactor: TransactionDetailInteractorProtocol

    @Published private(set) var model: TransactionDetailModel = TransactionDetailModel()

    init(interactor: TransactionDetailInteractorProtocol) {
        self.interactor = interactor
    }

    override func initialize() {

        interactor.models.sink {
            log.debug($0)
        }.store(in: &cancellableSet)

        interactor.models.assign(to: \.model, onUnowned: self).store(in: &cancellableSet)

        interactor.error.sink { [unowned self] error in
            self.alertMessage = AlertMessage(title: MainApp.Constants.Text.loadingFailedTitle,
                                             message: MainApp.Constants.Text.loadingFailedMessage)
        }.store(in: &cancellableSet)

        interactor.completed.map { _ in
            return false
        }.assign(to: \.isLoading, onUnowned: self).store(in: &cancellableSet)
    }

    func fetch() {
        isLoading = true
        interactor.fetch()
    }

}
