//
//  TransactionDetailModel.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionDetailModel: Identifiable {

    var id: String {
        transaction?.id ?? ""
    }

    var transaction: Transaction?
}
