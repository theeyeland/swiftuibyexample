//
//  ObjMapCombineAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject

public class TransactionDetailAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // DataProvider
        container.register(TransactionDetailDataProviderProtocol.self) { resolver in
            let dataProvider = TransactionDetailDataProvider()
            return dataProvider
        }

        // Interactor
        container.register(TransactionDetailInteractorProtocol.self) { resolver in
            let interactor = TransactionDetailInteractor(dataProvider: resolver.resolve(TransactionDetailDataProviderProtocol.self)!)
            interactor.data = self.data
            return interactor
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(TransactionDetailViewModel.self) { resolver in
            TransactionDetailViewModel(interactor: resolver.resolve(TransactionDetailInteractorProtocol.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        container.register(TransactionDetailView.self) { resolver in
            TransactionDetailView(viewModel: resolver.resolve(TransactionDetailViewModel.self)!)
        }
    }

    private var data: Transaction

    init(_ data: Transaction) {
        self.data = data
    }
}
