//
//  TransactionDetailInteractor.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import UIKit
import Combine

protocol TransactionDetailInteractorProtocol: BaseInteractorProtocol {

    var models: AnyPublisher<TransactionDetailModel, Never> { get }
    var error: AnyPublisher<MainAppError, Never> { get }
    var completed: AnyPublisher<Void, Never> { get }
    func fetch()
}

class TransactionDetailInteractor: BaseInteractor, TransactionDetailInteractorProtocol {

    private let dataProvider: TransactionDetailDataProviderProtocol

    var data: Transaction!

    init(dataProvider: TransactionDetailDataProviderProtocol) {
        self.dataProvider = dataProvider
    }

    var models: AnyPublisher<TransactionDetailModel, Never> {
        return self.dataProvider.fetch.values.eraseToAnyPublisher()
    }

    var error: AnyPublisher<MainAppError, Never> {
        return self.dataProvider.fetch.errors.eraseToAnyPublisher()
    }

    var completed: AnyPublisher<Void, Never> {
        return self.dataProvider.fetch.completed
    }

    override func initialize() {
    }

    func fetch() {
        if let data = data {
            dataProvider.fetch.apply(value: data)
        }
    }
}
