//
//  TransactionDetailDataProvider.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

protocol TransactionDetailDataProviderProtocol: BaseDataProviderProtocol {

    var fetch: Combine.Action<Transaction, TransactionDetailModel, MainAppError> {get}
}

class TransactionDetailDataProvider: BaseDataProvider, TransactionDetailDataProviderProtocol {

    lazy var fetch: Combine.Action<Transaction, TransactionDetailModel, MainAppError>  = {
        return Combine.Action<Transaction, TransactionDetailModel, MainAppError> { [unowned self] transaction in
            return Future<TransactionDetailModel, MainAppError> { [unowned self] promise in
                promise(.success(TransactionDetailModel(transaction: transaction)))
            }.eraseToAnyPublisher()
        }
    }()
}
