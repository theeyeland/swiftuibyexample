//
//  TransactionDetailView.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import SwiftUI

struct TransactionDetailView: View {

    @StateObject var viewModel: TransactionDetailViewModel

    var body: some View {
            ScrollView {
                if let transaction = viewModel.model.transaction {
                    VStack {
                        HStack {
                            Text(MainApp.Constants.Text.transactionBookingDateTitle)
                                .fontWeight(.bold)
                            Spacer()
                            Text("\(transaction.bookingDate.formattedAsRelativeDayName)")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                        HStack {
                            Text(MainApp.Constants.Text.transactionAccountNumberTitle)
                                .fontWeight(.bold)
                            Spacer()
                            Text(transaction.sender.acountNumber)
                                .foregroundColor(.secondary)
                        }
                        .padding()
                        TransactionRow(transaction: transaction)
                            .padding()
                    }
                } else {
                    EmptyView()
                }

                Spacer()
            }
            .navigationTitle(MainApp.Constants.Text.transactionDetailViewTitle)
            .onAppear {
                viewModel.fetch()
            }
    }
}

struct TransactionDetailView_Previews: PreviewProvider {

    static var previews: some View {
//        TransactionDetailView(transaction: TransactionsViewModel().models[0])
        EmptyView()
    }
}
