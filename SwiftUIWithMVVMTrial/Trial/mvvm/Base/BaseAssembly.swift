//
//  BaseAssembly.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 18.07.18.
//  Copyright © 2021 TheEyeLand All rights reserved.
//

import Foundation
import Swinject

public class BaseAssembly: Assembly, AssemblyDebugDeallocProtocol {

    deinit {
        log.theeyeland.debugDealloc(withObject: self)
    }

    public func assemble(container: Container) {
    }
}
