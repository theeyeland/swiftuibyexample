//
//  BaseViewModel.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import Foundation
import Combine

struct AlertMessage: Identifiable {

    var id: String { title }
    let title: String
    let message: String
}

class BaseViewModel: ViewModelDebugDeallocProtocol {

    lazy var cancellableSet: Set<AnyCancellable> = []

    @Published  var alertMessage: AlertMessage?
    @Published var isLoading: Bool = false

    deinit {
        log.theeyeland.debugDealloc(withObject: self)

        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func initialize() {}
}
