//
//  BaseDataProvider.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import Foundation
import Combine

protocol BaseDataProviderProtocol { }

class BaseDataProvider: NSObject, BaseDataProviderProtocol, DataProviderDebugDeallocProtocol {

    lazy var cancellableSet: Set<AnyCancellable> = []

    deinit {
        log.theeyeland.debugDealloc(withObject: self)
        cancellableSet.forEach {
            $0.cancel()
        }
    }
}
