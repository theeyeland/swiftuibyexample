//
//  BaseInteractor.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import Foundation
import Combine

protocol BaseInteractorProtocol {

    func initialize()
}

class BaseInteractor: InteractorDebugDeallocProtocol {

    lazy var cancellableSet: Set<AnyCancellable> = []
    lazy var notificationCenter: NotificationCenter = NotificationCenter.default

    deinit {
        log.theeyeland.debugDealloc(withObject: self)

        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func initialize() {

    }
}
