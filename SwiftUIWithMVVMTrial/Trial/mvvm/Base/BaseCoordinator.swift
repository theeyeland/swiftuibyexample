//
//  BaseCoordinator.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import Foundation
import Swinject

protocol BaseCoordinatorProtocol: class {

}

class BaseCoordinator: CoordinatorDebugDeallocProtocol {

    lazy var assembler: Assembler = Assembler.sharedAssembler
    lazy var notificationCenter: NotificationCenter = NotificationCenter.default

    deinit {
        log.theeyeland.debugDealloc(withObject: self)
    }
}
