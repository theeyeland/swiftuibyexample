//
//  TourScreenViewModel.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

class TourScreenViewModel: BaseViewModel, ObservableObject {

    private let interactor: TourScreenInteractorProtocol

    @Published private(set) var models: [TourScreenModel] = [TourScreenModel(text: "", tag: 0)]

    @Published var selectedTab: Int = 0

    init(interactor: TourScreenInteractorProtocol) {
        self.interactor = interactor
    }

    var lastPage: Bool {
        selectedTab == models.count - 1
    }

    override func initialize() {

        interactor.models.sink {
            log.debug($0)
        }.store(in: &cancellableSet)

        interactor.models.assign(to: \.models, onUnowned: self).store(in: &cancellableSet)

        interactor.error.sink { [unowned self] error in
            self.alertMessage = AlertMessage(title: MainApp.Constants.Text.loadingFailedTitle,
                                             message: MainApp.Constants.Text.loadingFailedMessage)
        }.store(in: &cancellableSet)

        interactor.completed.map { _ in
            return false
        }.assign(to: \.isLoading, onUnowned: self).store(in: &cancellableSet)
    }

    func fetch() {
        isLoading = true
        interactor.fetch()
    }
}
