//
//  TourScreenInteractor.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import UIKit
import Combine

protocol TourScreenInteractorProtocol: BaseInteractorProtocol {

    var models: AnyPublisher<[TourScreenModel], Never> { get }
    var error: AnyPublisher<MainAppError, Never> { get }
    var completed: AnyPublisher<Void, Never> { get }
    func fetch()
}

class TourScreenInteractor: BaseInteractor, TourScreenInteractorProtocol {

    private let dataProvider: TourScreenDataProviderProtocol

    init(dataProvider: TourScreenDataProviderProtocol) {
        self.dataProvider = dataProvider
    }

    var models: AnyPublisher<[TourScreenModel], Never> {
        return self.dataProvider.fetch.values.eraseToAnyPublisher()
    }

    var error: AnyPublisher<MainAppError, Never> {
        return self.dataProvider.fetch.errors.eraseToAnyPublisher()
    }

    var completed: AnyPublisher<Void, Never> {
        return self.dataProvider.fetch.completed
    }

    override func initialize() {
    }

    func fetch() {
        dataProvider.fetch.apply()
    }
}
