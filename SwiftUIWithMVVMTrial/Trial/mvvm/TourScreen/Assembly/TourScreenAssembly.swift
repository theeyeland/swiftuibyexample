//
//  ObjMapCombineAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject

public class TourScreenAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // DataProvider
        container.register(TourScreenDataProviderProtocol.self) { resolver in
            let dataProvider = TourScreenDataProvider()
            return dataProvider
        }

        // Interactor
        container.register(TourScreenInteractorProtocol.self) { resolver in
            return TourScreenInteractor(dataProvider: resolver.resolve(TourScreenDataProviderProtocol.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(TourScreenViewModel.self) { resolver in
            TourScreenViewModel(interactor: resolver.resolve(TourScreenInteractorProtocol.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        container.register(TourScreenView.self) { resolver in
            TourScreenView(viewModel: resolver.resolve(TourScreenViewModel.self)!)
        }
    }
}
