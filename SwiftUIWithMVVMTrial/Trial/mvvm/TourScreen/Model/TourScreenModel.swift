//
//  TourScreenModel.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct TourScreenModel: Identifiable {

    var id: String {
        text
    }

    var text: String = ""
    var tag: Int = 0
    var color: Color = .blue
}
