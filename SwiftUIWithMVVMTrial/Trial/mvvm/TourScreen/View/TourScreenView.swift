//
//  TourScreenView.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import SwiftUI

// A view modifier that detects shaking and calls a function of our choosing.
struct ScreenStyleModifier: ViewModifier {

    let color: Color

    func body(content: Content) -> some View {
            content
                .foregroundColor(color)
                .font(.largeTitle)
    }
}

// A View extension to make the modifier easier to use.
extension View {

    func screenStyle(color: Color = .blue) -> some View {
        self.modifier(ScreenStyleModifier(color: color))
    }
}

struct TourButton: View {

    @Environment(\.presentationMode) var presentationMode
    var title: String
    var textColor: Color

    var body: some View {
        Button {
            log.debug(title)
            presentationMode.wrappedValue.dismiss()
        } label: {
            Text(title)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 45)
                .font(.headline)
                .foregroundColor(textColor)
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 45)
        .background(Color.yellow)
        .cornerRadius(8)
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .stroke(Color.yellow, lineWidth: 1)
        )
        .padding(8)
    }
}

struct TourScreenView: View {

    @StateObject var viewModel: TourScreenViewModel

    var body: some View {
        VStack {
            TabView(selection: $viewModel.selectedTab) {
                ForEach(viewModel.models) { model in
                    Text(model.text)
                        .screenStyle(color: model.color)
                        .tag(model.tag)
                }
            }
            .background(Color.yellow)
            .ignoresSafeArea()
            .tabViewStyle(PageTabViewStyle())
            if viewModel.lastPage {
                TourButton(title: MainApp.Constants.Text.letsGo, textColor: viewModel.models[viewModel.selectedTab].color)
            } else {
                TourButton(title: MainApp.Constants.Text.skip, textColor: viewModel.models[viewModel.selectedTab].color)
            }
        }
        .onAppear {
            viewModel.fetch()
        }
        .onChange(of: viewModel.selectedTab) { value in
            log.debug("selected Tab: \(value)")

        }
    }
}

struct TourScreenView_Previews: PreviewProvider, AssemblerProtocol {
    static var previews: some View {
        TourScreenView(viewModel: resolver.resolve(TourScreenViewModel.self)!)
    }
}
