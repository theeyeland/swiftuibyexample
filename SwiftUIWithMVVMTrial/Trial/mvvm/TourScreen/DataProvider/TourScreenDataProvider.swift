//
//  TourScreenDataProvider.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

enum MainAppError: Error {
    case none
}

protocol TourScreenDataProviderProtocol: BaseDataProviderProtocol {

    var fetch: Combine.Action<Void, [TourScreenModel], MainAppError> {get}
}

class TourScreenDataProvider: BaseDataProvider, TourScreenDataProviderProtocol {

    lazy var fetch: Combine.Action<Void, [TourScreenModel], MainAppError>  = {
        return Combine.Action<Void, [TourScreenModel], MainAppError> { [unowned self] router in
            return Future<[TourScreenModel], MainAppError> { [unowned self] promise in
                promise(.success([TourScreenModel(text: MainApp.Constants.Text.screen1Message, tag: 0, color: .blue),
                                  TourScreenModel(text: MainApp.Constants.Text.screen2Message, tag: 1, color: .black),
                                  TourScreenModel(text: MainApp.Constants.Text.screen3Message, tag: 2, color: .green),
                                  TourScreenModel(text: MainApp.Constants.Text.screen4Message, tag: 3, color: .purple)]))
            }.eraseToAnyPublisher()
        }
    }()
}
