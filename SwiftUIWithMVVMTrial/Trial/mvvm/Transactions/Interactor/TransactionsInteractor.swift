//
//  TransactionsInteractor.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import UIKit
import Combine

protocol TransactionsInteractorProtocol: BaseInteractorProtocol {

    var models: AnyPublisher<[Transaction], Never> { get }
    var error: AnyPublisher<APIError, Never> { get }
    var completed: AnyPublisher<Void, Never> { get }
    func fetch()
}

class TransactionsInteractor: BaseInteractor, TransactionsInteractorProtocol {

    private let dataProvider: TransactionsDataProviderProtocol

    init(dataProvider: TransactionsDataProviderProtocol) {
        self.dataProvider = dataProvider
    }

    var models: AnyPublisher<[Transaction], Never> {
        return self.dataProvider.fetch.values.eraseToAnyPublisher()
    }

    var error: AnyPublisher<APIError, Never> {
        return self.dataProvider.fetch.errors.eraseToAnyPublisher()
    }

    var completed: AnyPublisher<Void, Never> {
        return self.dataProvider.fetch.completed
    }

    override func initialize() {
    }

    func fetch() {
        dataProvider.fetch.apply()
    }
}
