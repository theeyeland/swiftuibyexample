//
//  TransactionsDataProvider.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

protocol TransactionsDataProviderProtocol: BaseDataProviderProtocol {

    var fetch: Combine.Action<Void, [Transaction], APIError> {get}
}

class TransactionsDataProvider: BaseDataProvider, TransactionsDataProviderProtocol {

    private let transactionsAPI: TransactionsAPI

    init(transactionsAPI: TransactionsAPI) {
        self.transactionsAPI = transactionsAPI
    }

    lazy var fetch: Combine.Action<Void, [Transaction], APIError>  = {
        return Combine.Action<Void, [Transaction], APIError> { [unowned self] router in
            return Future<[Transaction], APIError> { [unowned self] promise in
                self.transactionsAPI.loadTransactions { [unowned self] (result) in
                    promise(result)
                }
            }.eraseToAnyPublisher()
        }
    }()
}
