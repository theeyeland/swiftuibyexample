//
//  TransactionsViewModel.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import Swinject

class TransactionsViewModel: BaseViewModel, ObservableObject {

    private let interactor: TransactionsInteractorProtocol
    private let coordinator: TransactionsCoordinator

    @Published private(set) var models: [Transaction] = []

    @Published var showTour: Bool = true

    init(interactor: TransactionsInteractorProtocol, coordinator: TransactionsCoordinator) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {

        interactor.models.sink { _ in
//            log.debug($0)
        }.store(in: &cancellableSet)

        interactor.models.assign(to: \.models, onUnowned: self).store(in: &cancellableSet)

        interactor.error.sink { [unowned self] error in
            self.alertMessage = AlertMessage(title: MainApp.Constants.Text.loadingFailedTitle,
                                             message: MainApp.Constants.Text.loadingFailedMessage)
        }.store(in: &cancellableSet)

        interactor.completed.map { _ in
            return false
        }.assign(to: \.isLoading, onUnowned: self).store(in: &cancellableSet)
    }

    func fetch() {
        isLoading = true
        interactor.fetch()
    }

    func demandForView(with format: TransactionsCoordinatorDemandFormat) -> some View {
        coordinator.demandForView(with: format)
    }
}
