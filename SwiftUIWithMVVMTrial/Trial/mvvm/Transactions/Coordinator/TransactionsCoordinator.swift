//
//  TransactionsCoordinator.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 17/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import UIKit
import Swinject
import SwiftUI

enum TransactionsCoordinatorDemandFormat {
    case tourScreen
    case transactionDetail(transaction: Transaction)
}

class TransactionsCoordinator: BaseCoordinator  {

    private func createTransactionDetail(with transaction: Transaction) -> some View {
        let assembly = TransactionDetailAssembly(transaction)
        assembler.apply(assembly: assembly)
        return assembler.resolver.resolve(TransactionDetailView.self)!
    }

    func demandForView(with format: TransactionsCoordinatorDemandFormat) -> some View {
        return Group {
            switch format {
            case .tourScreen:
                assembler.resolver.resolve(TourScreenView.self)!
            case .transactionDetail(let transaction) :
                createTransactionDetail(with: transaction)
            }
        }
    }
}
