//
//  TransactionsView.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 15/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import SwiftUI
import Swinject

struct NavigationLazyView<Content: View>: View {
    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    var body: Content {
        build()
    }
}

struct TransactionsView: View, AssemblerProtocol {

    @StateObject var viewModel: TransactionsViewModel

    var body: some View {
        NavigationView {
            ZStack {
                List {
                    Section {
                        HStack {
                            Text(MainApp.Constants.Text.transactionsSumTitle)
                                .font(.title2)
                                .fontWeight(.bold)
                            Spacer()
                            Text("\(viewModel.models.sum.formattedToEUR)")
                                .font(.title3)
                                .fontWeight(.bold)
                        }
                    }
                    ForEach(viewModel.models) { transaction in
                        NavigationLink( destination: NavigationLazyView(viewModel.demandForView(with: .transactionDetail(transaction: transaction)))) {
                            TransactionRow(transaction: transaction)
                        }
                    }
                }
                .navigationTitle(MainApp.Constants.Text.transactionsViewTitle) // it is called modifier as padding
                .listStyle(GroupedListStyle())
                .onAppear {
                    //                viewModel.fetch()
                }
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(MainApp.Constants.Text.transactionsTourButtonTitle) {
                            viewModel.showTour = true
                        }
                    }
                    ToolbarItemGroup(placement: .navigationBarTrailing) {
                        Button {
                            log.debug("Reload Transactions")
                            if !viewModel.isLoading {
                                viewModel.fetch()
                            }
                        } label: {
                            if viewModel.isLoading {
                                ProgressView()
                            } else {
                                Image(systemName: MainApp.Constants.Image.reloadSystemImage)
                            }
                        }
                        .foregroundColor(.primary)
                    }
                }
                .onChange(of: viewModel.showTour) {
                    if !$0 {
                        viewModel.fetch()
                    }
                }
                .alert(item: $viewModel.alertMessage) { message in
                    Alert(title: Text(message.title),
                          message: Text(message.message),
                          primaryButton: .default(Text(MainApp.Constants.Common.tryAgain)) {
                            log.debug("Try Again...")
                            viewModel.fetch()
                          },
                          secondaryButton: .cancel() {
                            log.debug("Canceled")
                          })
                }
                .sheet(isPresented: $viewModel.showTour) {
//                    TourScreenView(viewModel: resolver.resolve(TourScreenViewModel.self)!)
                    viewModel.demandForView(with: .tourScreen)
                }

                if viewModel.isLoading {
                    ProgressView()
                }
            }
        }
    }
}

struct TransactionsView_Previews: PreviewProvider, AssemblerProtocol {
    static var previews: some View {
        TransactionsView(viewModel: resolver.resolve(TransactionsViewModel.self)!)
    }
}
