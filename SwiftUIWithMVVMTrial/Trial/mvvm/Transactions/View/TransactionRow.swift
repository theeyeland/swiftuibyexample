//
//  TransactionRow.swift
//  Trial
//
//  Created by Zoltan Bognar The EyeLand on 16/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionRow: View {

    let transaction: Transaction

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                VStack(alignment: .leading) {
                    Text(transaction.title)
                        .font(.headline)
                    Spacer()
                    if let subtitle = transaction.subtitle {
                        Text(subtitle)
                            .font(.footnote)
                            .foregroundColor(.secondary)
                    }
                }
                ForEach(transaction.aditionalTextsLines, id: \.self) { item in
                    Text(item)
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
            }
            Spacer()
            Text("\(transaction.amount.formated)")
                .fontWeight(.semibold)
                .foregroundColor(transaction.amount.decimalValue.floatValue > 0 ? .green : .red)
        }

    }
}
