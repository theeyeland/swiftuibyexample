//
//  ObjMapCombineAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject

public class TransactionsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // TransactionsAPI
        container.register(TransactionsAPI.self) { resolver in
            TransactionsAPI()
        }

        // DataProvider
        container.register(TransactionsDataProviderProtocol.self) { resolver in
            let dataProvider = TransactionsDataProvider(transactionsAPI: resolver.resolve(TransactionsAPI.self)!)
            return dataProvider
        }

        // Interactor
        container.register(TransactionsInteractorProtocol.self) { resolver in
            return TransactionsInteractor(dataProvider: resolver.resolve(TransactionsDataProviderProtocol.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // Coordinator
        container.register(TransactionsCoordinator.self) { resolver in
            let coordinator = TransactionsCoordinator()
            return coordinator
        }

        // ViewModel
        container.register(TransactionsViewModel.self) { resolver in
            TransactionsViewModel(interactor: resolver.resolve(TransactionsInteractorProtocol.self)!,
                                  coordinator: resolver.resolve(TransactionsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        container.register(TransactionsView.self) { resolver in
            TransactionsView(viewModel: resolver.resolve(TransactionsViewModel.self)!)
        }
    }
}
