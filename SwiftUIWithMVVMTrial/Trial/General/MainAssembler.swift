//
//  MainAssembler.swift
//  WheeledMap
//
//  Created by Zoltan Bognar TheEyeLand on 11/06/2021.
//

import Foundation
import Swinject

extension Assembler {

    static let sharedAssembler: Assembler = {
        let container = Container()
        let assembler = Assembler([TransactionsAssembly(),
                                   TourScreenAssembly(),
                                   AppDelegateAssebly(),
                                   RootCoordinatorAssebly()
                                   ], container: container)
        return assembler
    }()
}

protocol AssemblerProtocol {

    static var assembler: Assembler {get}
    static var resolver: Resolver {get}

    var resolver: Resolver {get}
}

extension AssemblerProtocol {

    static var assembler: Assembler {
        Assembler.sharedAssembler
    }

    static var resolver: Resolver {
        assembler.resolver
    }

    var resolver: Resolver {
        Assembler.sharedAssembler.resolver
    }
}
