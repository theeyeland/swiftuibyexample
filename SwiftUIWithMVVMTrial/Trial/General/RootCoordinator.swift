//
//  RootCoordinator.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import UIKit
import Swinject
import SwiftUI

protocol RootCoordinatorProtocol: BaseCoordinatorProtocol {

    func present() -> UIHostingController<TransactionsView>
}

final class RootCoordinator: BaseCoordinator, RootCoordinatorProtocol {

    func present() -> UIHostingController<TransactionsView> {
        return UIHostingController(rootView: assembler.resolver.resolve(TransactionsView.self)!)
    }

}
