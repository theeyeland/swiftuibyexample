//
//  Constants.swift
//  WheeledMap
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2021 TheEyeLand. All rights reserved.
//

import Foundation

struct MainApp { }

extension MainApp {

    struct Constants {

        struct Logger {

            static let dataFlowTag = "dataFlowTag"
            static let mapping = "mapping"
        }

        struct Common {

            static var yes: String { return "Yes" }
            static var no: String { return "No" }
            static var ok: String { return "Ok" }
            static var done: String { return "Done" }
            static var cancel: String { return "Cancel" }
            static var close: String { return "Close" }
            static var edit: String { return "Edit" }
            static var delete: String { return "Delete" }
            static var confirm: String { return "Confirm" }
            static var tryAgain: String { return "Try Again" }
        }

        struct Text {

            // Alert
            static var loadingFailedTitle: String { return "Loading Failed"}
            static var loadingFailedMessage: String { return "Please try again"}
            // Tour Screen
            static var letsGo: String { return "Let's Go"}
            static var skip: String { return "Skip"}
            static var screen1Message: String { return "Welcome"}
            static var screen2Message: String { return "on Review"}
            static var screen3Message: String { return "of Trial App"}
            static var screen4Message: String { return "Enjoy :-)"}

            // Transactions
            static var transactionsViewTitle: String { return "Transactions"}
            static var transactionsSumTitle: String { return "Transactions Sum"}
            static var transactionsTourButtonTitle: String { return "Tour"}
            // Transaction Detail
            static var transactionDetailViewTitle: String { return "Transaction Detail"}
            static var transactionBookingDateTitle: String { return "Booking Date"}
            static var transactionAccountNumberTitle: String { return "Account Number"}
        }

        struct Image {

            static let reloadSystemImage: String = "arrow.triangle.2.circlepath"
        }
    }
}
