//
//  AppDelegate.swift
//  Trial
//
//  Created by Stefan Rinner on 12.02.18.
//  Copyright © 2018 BeeOne Gmbh. All rights reserved.
//

import UIKit
import SwiftUI
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var assembler = Assembler.sharedAssembler

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {

        _ = log

        log.outputLevel = .debug
        // Remove comment for view dealloc debug info only for mvvmc objects
        log.theeyeland.deallocFilter(withOtions: DebugDeallocOptions.mvvmc)

        let rootCoordinator = assembler.resolver.resolve(RootCoordinatorProtocol.self)!

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootCoordinator.present()
        window?.makeKeyAndVisible()

        return true
    }

}
