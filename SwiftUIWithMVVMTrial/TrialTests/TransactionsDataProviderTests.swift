//
//  TransactionsSusccessDataProviderTest.swift
//  TrialTests
//
//  Created by Zoltan Bognar The EyeLand on 17/06/2021.
//  Copyright © 2021 BeeOne Gmbh. All rights reserved.
//

import XCTest
import Swinject
import Combine

@testable import Trial

class TransactionsSuccessDataProvider: BaseDataProvider, TransactionsDataProviderProtocol {

    private let transactionsAPI: TransactionsAPI

    init(transactionsAPI: TransactionsAPI) {
        self.transactionsAPI = transactionsAPI
    }

    lazy var fetch: Combine.Action<Void, [Transaction], APIError>  = {
        return Combine.Action<Void, [Transaction], APIError> { [unowned self] router in
            return Future<[Transaction], APIError> { [unowned self] promise in
                self.transactionsAPI.loadTransactionsPreview { [unowned self] (result) in
                    promise(result)
                }
            }.eraseToAnyPublisher()
        }
    }()
}

class TransactionsErrorDataProvider: BaseDataProvider, TransactionsDataProviderProtocol {

    private let transactionsAPI: TransactionsAPI

    init(transactionsAPI: TransactionsAPI) {
        self.transactionsAPI = transactionsAPI
    }

    lazy var fetch: Combine.Action<Void, [Transaction], APIError>  = {
        return Combine.Action<Void, [Transaction], APIError> { [unowned self] router in
            return Future<[Transaction], APIError> { [unowned self] promise in
                self.transactionsAPI.loadTransactions { [unowned self] (result) in
                    promise(.failure(.randomError))
                }
            }.eraseToAnyPublisher()
        }
    }()
}

class TransactionsSusccessDataProviderTest: XCTestCase {

    var container: Container!
    var dataProvider: TransactionsDataProviderProtocol!
    lazy var cancellableSet: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        container = Container()
        // TransactionsAPI
        container.register(TransactionsAPI.self) { resolver in
            TransactionsAPI()
        }

        // DataProvider
        container.register(TransactionsDataProviderProtocol.self) { resolver in
            let dataProvider = TransactionsSuccessDataProvider(transactionsAPI: resolver.resolve(TransactionsAPI.self)!)
            return dataProvider
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        container = nil
        dataProvider = nil
        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func testFetchWithSuccess() throws {
        XCTContext.runActivity(named: "Fetch of Transactions should always succeed") { _ in
            // given
            dataProvider = container.resolve(TransactionsDataProviderProtocol.self)!
            let expectation = XCTestExpectation(description: "Load transactions with success")
            // when
            dataProvider.fetch.values.eraseToAnyPublisher().sink { transactions in
                expectation.fulfill()
            }.store(in: &cancellableSet)

            dataProvider.fetch.apply()
            // then
            wait(for: [expectation], timeout: 5.0)
        }

        XCTContext.runActivity(named: "Fetch of Transactions should have 45 values") { _ in
            // given
            dataProvider = container.resolve(TransactionsDataProviderProtocol.self)!
            let expectation = XCTestExpectation(description: "Transactions count = 45")
            // when
            dataProvider.fetch.values.eraseToAnyPublisher().sink { transactions in
                if transactions.count == 45 {
                    expectation.fulfill()
                }
            }.store(in: &cancellableSet)

            dataProvider.fetch.apply()
            // then
            wait(for: [expectation], timeout: 5.0)
        }
    }

    func testTransuctionsSum() throws {

        XCTContext.runActivity(named: "Sum of all transaction should be negative value") { _ in
            // given
            dataProvider = container.resolve(TransactionsDataProviderProtocol.self)!
            let expectation = XCTestExpectation(description: "Transactions Sum as negative value")
            // when
            dataProvider.fetch.values.eraseToAnyPublisher().sink { transactions in
                if transactions.sum < 0 {
                    expectation.fulfill()
                }
            }.store(in: &cancellableSet)

            dataProvider.fetch.apply()
            // then
            wait(for: [expectation], timeout: 5.0)
        }
    }
}

class TransactionsErrorDataProviderTest: XCTestCase {

    var container: Container!
    var dataProvider: TransactionsDataProviderProtocol!
    lazy var cancellableSet: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        container = Container()

        // TransactionsAPI
        container.register(TransactionsAPI.self) { resolver in
            TransactionsAPI()
        }

        // DataProvider
        container.register(TransactionsDataProviderProtocol.self) { resolver in
            let dataProvider = TransactionsErrorDataProvider(transactionsAPI: resolver.resolve(TransactionsAPI.self)!)
            return dataProvider
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        container = nil
        dataProvider = nil
        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func testFetchWithError() throws {
        XCTContext.runActivity(named: "Fetch of Transactions should always give error") { _ in
            // given
            dataProvider = container.resolve(TransactionsDataProviderProtocol.self)!
            let expectation = XCTestExpectation(description: "Load transactions with error")
            // when
            dataProvider.fetch.errors.eraseToAnyPublisher().sink { transactions in
                expectation.fulfill()
            }.store(in: &cancellableSet)

            dataProvider.fetch.apply()
            // then
            wait(for: [expectation], timeout: 5.0)
        }
    }
}
