//
//  TrialTests.swift
//  TrialTests
//
//  Created by Stefan Rinner on 12.02.18.
//  Copyright © 2018 BeeOne Gmbh. All rights reserved.
//

import XCTest
import Combine
import Swinject

@testable import Trial

class TrialTests: XCTestCase {

    var container: Container!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        container = Container()
        // TransactionsAPI
        container.register(TransactionsAPI.self) { resolver in
            TransactionsAPI()
        }

        // DataProvider
        container.register(TransactionsDataProviderProtocol.self) { resolver in
            let dataProvider = TransactionsSuccessDataProvider(transactionsAPI: resolver.resolve(TransactionsAPI.self)!)
            return dataProvider
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        container = nil
    }

    func testTransactionParsing() {
        let api = TransactionsAPI()

        let expectation = XCTestExpectation(description: "Load transactions")

        api.loadTransactions { result in
            switch result {
            case .success(let transactions):
                XCTAssertEqual(45, transactions.count)
            case .failure:
                break
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }
}
