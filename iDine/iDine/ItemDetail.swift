//
//  ItemDetail.swift
//  iDine
//
//  Created by Zoltan Bognar The EyeLand on 13/05/2021.
//

import SwiftUI

struct ItemDetail: View {

    @EnvironmentObject var order: Order
    let item: MenuItem

    var body: some View {
//        GeometryReader { geometry in // not working as I expected
            ScrollView {
                VStack {
                    ZStack(alignment: .bottomTrailing) {
                        Image(item.mainImage)
                            .resizable()
                            .scaledToFit()
                        Text("Photo: \(item.photoCredit)")
                            .padding(4)
                            .background(Color.black)
                            .font(.caption)
                            .foregroundColor(.white)
                            .offset(x: -5, y: -5)
                    }
                    Text(item.description)
                        .padding()

                    Button() {
                        order.add(item: item)
                    } label: {
                        Text("Order This")
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 45)
                            .font(.headline)
                            .foregroundColor(.white)
                    }
//                    .frame(width: geometry.size.width - 16)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 45)
                    .background(Color.gray)
                    .cornerRadius(8)
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.black, lineWidth: 1)
                    )
                    .padding(8)
//                    .offset(x: 1.0, y: 0)
                    Spacer()
                }
            }
//        }
        .navigationTitle(item.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct ItemDetail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ItemDetail( item: MenuItem.example).environmentObject(Order())
        }
    }
}
